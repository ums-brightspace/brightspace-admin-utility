package main

import (
	"github.com/joho/godotenv"
	"gitlab.its.maine.edu/development/lms/utility/server"
)

func main() {
	_ = godotenv.Load()
	server.RunServer()
	server.OnSigKillTerminate()
}
