export default {
	delete_adminUserPermission({ dispatch }, payload) {
		this.$axios
			.$delete(
				"/adminUserPermission/" +
					payload.adminUserId +
					"/" +
					payload.adminPermissionId +
					"/"
			)
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
				dispatch(
					"System/AdminUserPermissions/get_adminUser",
					{ id: payload.adminUserId },
					{ root: true }
				);
			});
	},

	add_adminUserPermission({ dispatch }, payload) {
		this.$axios
			.$post("/adminUserPermission/", payload)
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
				dispatch(
					"System/AdminUserPermissions/get_adminUser",
					{ id: payload.adminUserId },
					{ root: true }
				);
			});
	},
};
