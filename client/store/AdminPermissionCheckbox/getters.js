export default {
	adminPermissionCheckboxValue(state) {
		return (name) => {
			for (let i = 0; i < state.adminPermissionCheckboxValues.length; i++) {
				if (state.adminPermissionCheckboxValues[i].name === name) {
					return state.adminPermissionCheckboxValues[i].value;
				}
			}

			return false;
		};
	},
};
