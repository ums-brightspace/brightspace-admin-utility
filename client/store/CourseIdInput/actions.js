export default {
	get_course_title(store, course_lms_id) {
		let vThis = this;
		return new Promise(function (resolve, reject) {
			vThis.$axios
				.$get("/lmsCourse/" + course_lms_id)
				.then(function (res) {
					resolve(res.Name);
				})
				.catch(function (err) {
					reject(err);
				});
		});
	},
};
