export default {
	campusDdlVisited(state) {
		return state.campusDdlVisited;
	},

	campus(state) {
		return state.campus;
	},

	semester_visited(state) {
		return state.semester_visited;
	},

	year_visited(state) {
		return state.year_visited;
	},

	semester(state) {
		return state.semester;
	},

	year(state) {
		return state.year;
	},

	term(state) {
		return state.term;
	},

	selected_course_code(state) {
		return state.selected_course_code;
	},

	selected_emplid(state) {
		return state.selected_emplid;
	},

	selected_username(state) {
		return state.selected_username;
	},
};
