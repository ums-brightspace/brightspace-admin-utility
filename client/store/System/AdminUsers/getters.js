export default {
	adminUsers(state) {
		return state.adminUsers;
	},

	adminUsersFetching(state) {
		return state.adminUsersFetching;
	},
};
