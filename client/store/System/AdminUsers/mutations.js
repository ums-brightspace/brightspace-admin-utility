export default {
	set_adminUsers(state, payload) {
		state.adminUsers = payload;
	},
	set_adminUsersFetching(state, payload) {
		state.adminUsersFetching = payload;
	},
};
