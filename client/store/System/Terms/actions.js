export default {
	get_termsToProcess({ commit }) {
		commit("set_termsFetching", true);
		this.$axios
			.$get("/termsToProcess/")
			.then(function (res) {
				commit("set_termsToProcess", res);
				commit("set_termsFetching", false);
			})
			.catch(function () {
				commit("set_termsToProcess", []);
			});
	},

	delete_termsToProcess({ dispatch, commit, getters }, payload) {
		this.$axios
			.$delete("/termsToProcess/" + payload.institution + "/" + payload.term)
			.then(function () {
				dispatch("get_coursesStats");
				let terms = getters.termsToProcess;
				terms.splice(
					terms.findIndex(
						(t) =>
							t.institution === payload.institution && t.term === payload.term
					),
					1
				);
				commit("set_termsToProcess", terms);
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			});
	},

	add_termsToProcess({ dispatch }, payload) {
		let ttp = {
			institution: payload.institution,
			term: payload.term,
		};
		this.$axios
			.$post("/termsToProcess/", ttp)
			.then(function () {
				dispatch("get_coursesStats");
				dispatch("get_termsToProcess");
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			});
	},

	get_coursesStats({ commit }) {
		commit("set_statsFetching", true);
		this.$axios
			.$get("/courses/stats/")
			.then(function (res) {
				commit("set_coursesStats", res);
				commit("set_statsFetching", false);
			})
			.catch(function () {
				commit("set_coursesStats", null);
			});
	},
};
