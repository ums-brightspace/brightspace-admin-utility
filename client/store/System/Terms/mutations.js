export default {
	set_coursesStats(state, payload) {
		state.coursesStats = payload;
	},

	set_termsToProcess(state, payload) {
		state.termsToProcess = payload;
	},

	set_statsFetching(state, payload) {
		state.statsFetching = payload;
	},

	set_termsFetching(state, payload) {
		state.termsFetching = payload;
	},
};
