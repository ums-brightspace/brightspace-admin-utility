export default {
	set_colleges_all(state, payload) {
		state.colleges_all = payload;
	},

	set_academic_units_all(state, payload) {
		state.academic_units_all = payload;
	},
};
