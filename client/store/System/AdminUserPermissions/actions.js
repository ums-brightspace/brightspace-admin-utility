export default {
	get_adminUser({ commit }, payload) {
		commit("set_adminUserFetching", true);
		this.$axios
			.$get("/adminUser/" + payload.id + "/")
			.then(function (res) {
				commit("set_adminUser", res);

				commit("AdminPageCheckbox/clear_adminPageCheckboxValues", null, {
					root: true,
				});
				let pages = res.allowedAdminPages;
				if (pages) {
					pages.forEach(function (page) {
						commit(
							"AdminPageCheckbox/set_adminPageCheckboxValue",
							{
								routeName: page.routeName,
								value: true,
							},
							{ root: true }
						);
					});
				}

				commit(
					"AdminPermissionCheckbox/clear_adminPermissionCheckboxValues",
					null,
					{ root: true }
				);
				let perms = res.allowedAdminPermissions;
				if (perms) {
					perms.forEach(function (perm) {
						commit(
							"AdminPermissionCheckbox/set_adminPermissionCheckboxValue",
							{
								name: perm.name,
								value: true,
							},
							{ root: true }
						);
					});
				}
			})
			.catch(function () {
				commit("set_adminUser", null);
			})
			.finally(function () {
				commit("set_adminUserFetching", false);
			});
	},

	get_adminPagesWithPermissions({ commit }) {
		commit("set_adminPagesPermissionsFetching", true);
		this.$axios
			.$get("/adminPage/all-with-permissions/")
			.then(function (res) {
				commit("set_adminPagesPermissions", res);
			})
			.catch(function () {
				commit("set_adminPagesPermissions", []);
			})
			.finally(function () {
				commit("set_adminPagesPermissionsFetching", false);
			});
	},
};
