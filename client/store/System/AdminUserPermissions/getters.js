export default {
	adminUsersPermissions(state) {
		return state.adminUsersPermissions;
	},

	adminUsersPermissionsFetching(state) {
		return state.adminUsersPermissionsFetching;
	},

	adminPagesPermissions(state) {
		return state.adminPagesPermissions;
	},

	adminPagesPermissionsFetching(state) {
		return state.adminPagesPermissionsFetching;
	},

	adminUser(state) {
		return state.adminUser;
	},

	adminUserFetching(state) {
		return state.adminUserFetching;
	},
};
