export default () => ({
	adminPagesPermissions: [],
	adminPagesPermissionsFetching: false,
	adminUser: null,
	adminUserFetching: false,
});
