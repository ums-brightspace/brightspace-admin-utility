export default {
	set_adminUsersPermissions(state, payload) {
		state.adminUsersPermissions = payload;
	},

	set_adminUsersPermissionsFetching(state, payload) {
		state.adminUsersPermissionsFetching = payload;
	},

	set_adminPagesPermissions(state, payload) {
		state.adminPagesPermissions = payload;
	},

	set_adminPagesPermissionsFetching(state, payload) {
		state.adminPagesPermissionsFetching = payload;
	},

	set_adminUser(state, payload) {
		state.adminUser = payload;
	},

	set_adminUserFetching(state, payload) {
		state.adminUserFetching = payload;
	},
};
