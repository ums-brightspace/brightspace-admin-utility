export default {
	set_environment(state, payload) {
		state.environment = payload;
	},

	set_username(state, payload) {
		state.username = payload;
	},

	set_showAdminHelpInfo(state, payload) {
		state.showAdminHelpInfo = payload;
	},

	set_permissions(state, payload) {
		state.permissions = payload;
	},

	set_pages(state, payload) {
		state.pages = payload;
	},
};
