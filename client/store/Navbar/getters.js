export default {
	environment(state) {
		return state.environment;
	},

	username(state) {
		return state.username;
	},

	showAdminHelpInfo(state) {
		return state.showAdminHelpInfo;
	},

	permissions(state) {
		return state.permissions;
	},

	hasPermission(state) {
		return (permission) => {
			for (let i = 0; i < state.permissions.length; i++) {
				if (state.permissions[i].name === permission) {
					return true;
				}
			}

			return false;
		};
	},

	pages(state) {
		return state.pages;
	},

	hasPage(state) {
		return (page) => {
			for (let i = 0; i < state.pages.length; i++) {
				if (state.pages[i].routeName === page) {
					return true;
				}
			}

			return false;
		};
	},
	lms_host(state) {
		if (state.environment === "Prod") {
			return "courses.maine.edu";
		}

		return "mainetest.brightspace.com";
	},
	lms_url(state, getters) {
		return "https://" + getters.lms_host + "/d2l";
	},
	lms_url_for_course(state, getters) {
		return (courseLmsId) => {
			return "/home" + getters.lms_url + "/" + courseLmsId;
		};
	},
};
