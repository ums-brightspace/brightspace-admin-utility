export default {
	set_role_overrides(state, payload) {
		state.role_overrides = payload;
	},

	set_fetching(state, payload) {
		state.fetching = payload;
	},

	set_roles(state, payload) {
		state.roles = payload;
	},
};
