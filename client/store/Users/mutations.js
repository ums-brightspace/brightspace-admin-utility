export default {
	set_ms_user_info(state, payload) {
		state.ms_user_info = payload;
	},

	set_ms_user_info_fetching(state, payload) {
		state.ms_user_info_fetching = payload;
	},

	set_bs_user_info(state, payload) {
		state.bs_user_info = payload;
	},

	set_bs_user_info_fetching(state, payload) {
		state.bs_user_info_fetching = payload;
	},
};
