export default {
	set_user_enrollments_system(state, payload) {
		state.user_enrollments_system = payload;
	},

	set_user_enrollments_system_fetching(state, payload) {
		state.user_enrollments_system_fetching = payload;
	},
};
