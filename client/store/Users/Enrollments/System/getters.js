export default {
	user_enrollments_system(state) {
		return state.user_enrollments_system;
	},

	user_enrollments_system_fetching(state) {
		return state.user_enrollments_system_fetching;
	},
};
