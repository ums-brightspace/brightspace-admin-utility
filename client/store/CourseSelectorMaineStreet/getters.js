export default {
	coursesMS(state) {
		return state.coursesMS;
	},

	courses_fetching(state) {
		return state.courses_fetching;
	},

	search_subject(state) {
		return state.search_subject;
	},

	search_number(state) {
		return state.search_number;
	},
};
