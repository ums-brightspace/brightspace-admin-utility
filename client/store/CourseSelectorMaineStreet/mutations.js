export default {
	set_coursesMS(state, payload) {
		state.coursesMS = payload;
	},

	set_courses_fetching(state, payload) {
		state.courses_fetching = payload;
	},

	set_search_subject(state, payload) {
		state.search_subject = payload;
	},

	set_search_number(state, payload) {
		state.search_number = payload;
	},
};
