export default {
	delete_alert({ commit, getters }, alert_id) {
		let alerts = getters.alerts;
		for (let i = 0; i < alerts.length; i++) {
			if (alerts[i].id === alert_id) {
				alerts.splice(i, 1);
				break;
			}
		}

		commit("set_alerts", alerts);
	},

	add_alert({ commit, getters }, payload) {
		// example expected payload = {
		// 	variant: 'one of https://bootstrap-vue.js.org/docs/reference/color-variants',
		// 	msg: 'the message'
		// };

		payload.id = uuid();

		let alerts = getters.alerts;
		alerts.push(payload);

		commit("set_alerts", alerts);
	},
};

function uuid(
	a // placeholder
) {
	return a // if the placeholder was passed, return
		? // a random number from 0 to 15
		  (
				a ^ // unless b is 8,
				((Math.random() * // in which case
					16) >> // a random number from
					(a / 4))
		  ) // 8 to 11
				.toString(16) // in hexadecimal
		: // or otherwise a concatenated string:
		  (
				[1e7] + // 10000000 +
				-1e3 + // -1000 +
				-4e3 + // -4000 +
				-8e3 + // -80000000 +
				-1e11
		  ) // -100000000000,
				.replace(
					// replacing
					/[018]/g, // zeroes, ones, and eights with
					uuid // random hex digits
				);
}
