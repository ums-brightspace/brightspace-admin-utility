export default {
	set_campusDdlVisited(state, isVisited) {
		state.campusDdlVisited = isVisited;
	},

	set_campus(state, isVisited) {
		state.campus = isVisited;
	},

	set_semester_visited(state, isVisited) {
		state.semester_visited = isVisited;
	},

	set_year_visited(state, isVisited) {
		state.year_visited = isVisited;
	},

	set_semester(state, payload) {
		state.semester = payload;
	},

	set_year(state, payload) {
		state.year = payload;
	},

	set_term(state, payload) {
		state.term = payload;
	},

	set_selected_course_code(state, payload) {
		state.selected_course_code = payload;
	},
	set_selected_emplid(state, payload) {
		state.selected_emplid = payload;
	},
	set_selected_username(state, payload) {
		state.selected_username = payload;
	},
};
