export default () => ({
	campusDdlVisited: false,
	campus: null,
	semester_visited: false,
	year_visited: false,
	semester: null,
	year: "",
	term: "",
	selected_course_code: "",
	selected_emplid: "",
	selected_username: "",
});
