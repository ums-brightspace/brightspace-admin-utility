export default {
	set_universities(state, payload) {
		state.universities = payload;
	},

	set_colleges(state, payload) {
		state.colleges = payload;
	},

	set_academic_units(state, payload) {
		state.academic_units = payload;
	},
};
