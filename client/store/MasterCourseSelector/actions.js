export default {
	get_universities({ commit }) {
		this.$axios
			.$get("university/all/")
			.then(function (res) {
				commit("set_universities", res);
			})
			.catch(function () {
				commit("set_universities", []);
			});
	},

	get_colleges({ commit }) {
		this.$axios
			.$get("college/set/")
			.then(function (res) {
				commit("set_colleges", res);
			})
			.catch(function () {
				commit("set_colleges", []);
			});
	},

	get_academic_units({ commit }) {
		this.$axios
			.$get("academicUnit/set/")
			.then(function (res) {
				commit("set_academic_units", res);
			})
			.catch(function () {
				commit("set_academic_units", []);
			});
	},

	create_dev_course({ dispatch, getters }, payload) {
		this.$axios
			.$post("lmsCourse/dev/", payload)
			.then(function (res) {
				let courseType = payload.isMaster
					? "Master course "
					: "Development course ";
				//todo: can we move host name to .env ?
				let host = "mainetest.brightspace.com";
				if (getters.environment === "Prod") {
					host = "courses.maine.edu";
				}
				let url = "https://" + host + "/d2l/home/" + res.Identifier;
				let msg =
					courseType +
					", <a href=" +
					url +
					" target='_blank' >" +
					payload.code +
					"</a>" +
					", created ";

				dispatch(
					"Alerts/add_alert",
					{
						variant: "success",
						msg: msg,
					},
					{ root: true }
				);
			})
			.catch(function (error) {
				let errMessage = "";
				if (error.response) {
					errMessage = error.response.data;
				} else if (error.request) {
					errMessage = error.request;
				} else {
					errMessage = error.message;
				}
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg:
							"Unable to create the course, " +
							payload.code +
							": " +
							errMessage,
					},
					{ root: true }
				);
			});
	},
};
