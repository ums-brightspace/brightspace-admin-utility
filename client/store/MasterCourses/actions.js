export default {
	get_colleges_all({ commit }) {
		this.$axios
			.$get("college/all/")
			.then(function (res) {
				commit("set_colleges_all", res);
			})
			.catch(function () {
				commit("set_colleges_all", []);
			});
	},

	get_academic_units_all({ commit }) {
		this.$axios
			.$get("academicUnit/all/")
			.then(function (res) {
				commit("set_academic_units_all", res);
			})
			.catch(function () {
				commit("set_academic_units_all", []);
			});
	},

	update_master_course({ dispatch }, payload) {
		let msgPart = "saving " + payload.course_id + " for " + payload.type;
		if (payload.course_id === "") {
			msgPart = "removing master course from " + payload.type;
		}

		this.$axios
			.$put(payload.type + "/" + payload.id + "/", {
				template_lms_id: payload.course_id,
				term: payload.term,
			})
			.then(function () {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "success",
						msg: "Success " + msgPart,
					},
					{ root: true }
				);
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "error",
						msg: "Failure " + msgPart + " with error: " + resError,
					},
					{ root: true }
				);
			});
	},
};
