export default {
	showCampusDdlErrors({ commit }) {
		commit("set_campusDdlVisited", true);
	},
	hideCampusDdlErrors({ commit }) {
		commit("set_campusDdlVisited", false);
	},
	showTermSelectorErrors({ commit }) {
		commit("set_semester_visited", true);
		commit("set_year_visited", true);
	},
	hideTermSelectorErrors({ commit }) {
		commit("set_semester_visited", false);
		commit("set_year_visited", false);
	},
};
