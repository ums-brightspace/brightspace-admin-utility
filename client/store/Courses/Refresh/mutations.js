export default {
	set_ms_title(state, payload) {
		state.ms_title = payload;
	},

	set_ms_title_fetching(state, payload) {
		state.ms_title_fetching = payload;
	},

	set_bs_title(state, payload) {
		state.bs_title = payload;
	},

	set_bs_title_fetching(state, payload) {
		state.bs_title_fetching = payload;
	},
};
