export default {
	get_ms_title({ commit }, courseCode) {
		commit("set_ms_title_fetching", true);
		this.$axios
			.$get("/course/byCode/?code=" + courseCode)
			.then(function (res) {
				commit("set_ms_title", res.title);
				commit("set_ms_title_fetching", false);
			})
			.catch(function () {
				commit("set_ms_title", null);
				commit("set_ms_title_fetching", false);
			});
	},

	get_bs_title({ commit }, courseCode) {
		commit("set_bs_title_fetching", true);
		this.$axios
			.$get("/lmsCourse/byCode/?code=" + courseCode)
			.then(function (res) {
				commit("set_bs_title", res.Name);
				commit("set_bs_title_fetching", false);
			})
			.catch(function () {
				commit("set_bs_title", null);
				commit("set_bs_title_fetching", false);
			});
	},

	update_bs_title({ commit, dispatch }, courseCode) {
		commit("set_bs_title_fetching", true);
		this.$axios
			.$put("/lmsCourse/byCode/?code=" + courseCode)
			.then(function () {
				dispatch("get_bs_title", courseCode);
			})
			.catch(function () {
				commit("set_bs_title", null);
				commit("set_bs_title_fetching", false);
			});
	},
};
