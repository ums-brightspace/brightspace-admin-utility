export default {
	ms_title(state) {
		return state.ms_title;
	},

	ms_title_fetching(state) {
		return state.ms_title_fetching;
	},

	bs_title(state) {
		return state.bs_title;
	},

	bs_title_fetching(state) {
		return state.bs_title_fetching;
	},
};
