export default {
	bs_course_lms_id(state) {
		return state.bs_course_lms_id;
	},

	bs_course_lms_id_fetching(state) {
		return state.bs_course_lms_id_fetching;
	},
};
