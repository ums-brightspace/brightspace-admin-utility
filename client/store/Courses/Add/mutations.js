export default {
	set_bs_course_lms_id(state, payload) {
		state.bs_course_lms_id = payload;
	},

	set_bs_course_lms_id_fetching(state, payload) {
		state.bs_course_lms_id_fetching = payload;
	},
};
