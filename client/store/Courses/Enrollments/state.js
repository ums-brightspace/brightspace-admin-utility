export default () => ({
	ms_enrollments: [],
	ms_enrollments_fetching: false,
	bs_enrollments_fetching: false,
	bs_enrollments: [],
	changed_enrollments: [],
});
