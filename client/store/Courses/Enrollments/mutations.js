export default {
	set_ms_enrollments(state, payload) {
		state.ms_enrollments = payload;
	},

	set_ms_enrollments_fetching(state, payload) {
		state.ms_enrollments_fetching = payload;
	},

	set_bs_enrollments(state, payload) {
		state.bs_enrollments = payload;
	},

	set_bs_enrollments_fetching(state, payload) {
		state.bs_enrollments_fetching = payload;
	},

	set_changed_enrollments(state, payload) {
		state.changed_enrollments = payload;
	},

	push_changed_enrollments(state, payload) {
		let changed_enrolls = state.changed_enrollments;
		let foundI = changed_enrolls.findIndex((e) => e.emplid === payload.emplid);
		if (foundI !== -1) {
			changed_enrolls.splice(foundI, 1);
		}

		changed_enrolls.push(payload);

		state.changed_enrollments = changed_enrolls;
	},
};
