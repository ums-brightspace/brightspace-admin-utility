export default {
	fetching(state) {
		return state.fetching;
	},

	courses(state) {
		return state.courses;
	},
};
