export default {
	get_courses({ commit }, payload) {
		commit("set_courses", []);
		commit("set_fetching", true);
		this.$axios
			.$get(
				`/courses/by-acad-org/?campus=${payload.campus}&term=${payload.term}&acadOrg=${payload.acadOrg}`
			)
			.then(function (res) {
				commit("set_courses", res);
				commit("set_fetching", false);
			})
			.catch(function () {
				commit("set_courses", []);
				commit("set_fetching", false);
			});
	},
	request_course_copies({ dispatch }, payload) {
		this.$axios.$post(`/courses/bulk-copy/`, payload).catch(function (error) {
			dispatch(
				"Alerts/add_alert",
				{
					variant: "warning",
					msg: "Unable to request one or more of the course copies: " + error,
				},
				{ root: true }
			);
		});
	},
};
