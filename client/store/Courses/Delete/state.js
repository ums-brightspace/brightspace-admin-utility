export default () => ({
	bs_delete_course_lms_id_fetching: false,
	bs_delete_course_lms_id: "",
	bs_recreate_course_lms_id: "",
	bs_recreate_course_lms_id_fetching: false,
});
