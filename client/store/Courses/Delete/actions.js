export default {
	// todo: repeated code with courses/add
	get_bs_delete_course_lms_id({ commit }, courseCode) {
		commit("set_bs_delete_course_lms_id_fetching", true);
		commit("set_bs_recreate_course_lms_id_fetching", true);
		this.$axios
			.$get("/lmsCourse/byCode/?code=" + courseCode)
			.then(function (res) {
				commit("set_bs_delete_course_lms_id", res.Identifier);
				commit("set_bs_delete_course_lms_id_fetching", false);
			})
			.catch(function () {
				commit("set_bs_delete_course_lms_id", null);
				commit("set_bs_delete_course_lms_id_fetching", false);
			});
	},

	delete_course({ dispatch }, courseCode) {
		let vThis = this;
		return new Promise(function (resolve, reject) {
			vThis.$axios
				.$delete("/lmsCourse/byCode/?code=" + courseCode)
				.then(function (res) {
					resolve(res.Identifier);
				})
				.catch(function (err) {
					dispatch(
						"Alerts/add_alert",
						{
							variant: "warning",
							msg: "Unable to delete the course, " + courseCode + ". " + err,
						},
						{ root: true }
					);
					reject(err);
				});
		});
	},

	recreate_course({ commit, dispatch }, courseCode) {
		let vThis = this;
		return new Promise(function (resolve, reject) {
			commit("set_bs_recreate_course_lms_id_fetching", true);
			vThis.$axios
				.$post("/lmsCourse/", { code: courseCode })
				.then(function (res) {
					commit("set_bs_recreate_course_lms_id", res.Identifier);
					commit("set_bs_recreate_course_lms_id_fetching", false);
					resolve();
				})
				.catch(function (err) {
					commit("set_bs_recreate_course_lms_id", null);
					commit("set_bs_recreate_course_lms_id_fetching", false);
					dispatch(
						"Alerts/add_alert",
						{
							variant: "warning",
							msg:
								"Unable to recreate the course, " +
								courseCode +
								". " +
								err +
								" It will be created by the integration",
						},
						{ root: true }
					);
					reject(err);
				});
		});
	},
};
