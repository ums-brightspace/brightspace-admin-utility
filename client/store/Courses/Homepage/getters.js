export default {
	change_course_homepage_fetching(state) {
		return state.change_course_homepage_fetching;
	},

	change_course_homepage_errors(state) {
		return state.change_course_homepage_errors;
	},

	change_course_homepage_in_progress(state) {
		return state.change_course_homepage_in_progress;
	},
};
