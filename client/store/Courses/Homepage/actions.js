export default {
	update_course_homepages({ commit }, payload) {
		commit("set_change_course_homepage_fetching", true);
		commit("set_change_course_homepage_in_progress", true);
		commit("set_change_course_homepage_errors", null);
		this.$axios
			.$post("/courses/homepages/", payload)
			.then(function () {
				commit("change_course_homepage_errors", null);
				commit("set_change_course_homepage_fetching", false);
			})
			.catch(function (err) {
				commit("set_change_course_homepage_errors", err.response.data);
				commit("set_change_course_homepage_fetching", false);
			});
	},
};
