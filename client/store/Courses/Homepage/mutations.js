export default {
	set_change_course_homepage_fetching(state, payload) {
		state.change_course_homepage_fetching = payload;
	},

	set_change_course_homepage_errors(state, payload) {
		state.change_course_homepage_errors = payload;
	},

	set_change_course_homepage_in_progress(state, payload) {
		state.change_course_homepage_in_progress = payload;
	},
};
