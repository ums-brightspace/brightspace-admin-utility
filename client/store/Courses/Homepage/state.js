export default () => ({
	change_course_homepage_fetching: false,
	change_course_homepage_errors: null,
	change_course_homepage_in_progress: false,
});
