export default {
	update_course_navbars({ commit }, payload) {
		commit("set_change_course_navbar_fetching", true);
		commit("set_change_course_navbar_in_progress", true);
		commit("set_change_course_navbar_errors", null);
		this.$axios
			.$post("/courses/navbars/", payload)
			.then(function () {
				commit("change_course_navbar_errors", null);
				commit("set_change_course_navbar_fetching", false);
			})
			.catch(function (err) {
				commit("set_change_course_navbar_errors", err.response.data);
				commit("set_change_course_navbar_fetching", false);
			});
	},
};
