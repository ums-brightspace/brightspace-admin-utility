export default {
	change_course_navbar_fetching(state) {
		return state.change_course_navbar_fetching;
	},

	change_course_navbar_errors(state) {
		return state.change_course_navbar_errors;
	},

	change_course_navbar_in_progress(state) {
		return state.change_course_navbar_in_progress;
	},
};
