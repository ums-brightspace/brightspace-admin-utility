export default {
	set_change_course_navbar_fetching(state, payload) {
		state.change_course_navbar_fetching = payload;
	},

	set_change_course_navbar_errors(state, payload) {
		state.change_course_navbar_errors = payload;
	},

	set_change_course_navbar_in_progress(state, payload) {
		state.change_course_navbar_in_progress = payload;
	},
};
