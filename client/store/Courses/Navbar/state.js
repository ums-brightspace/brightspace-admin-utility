export default () => ({
	change_course_navbar_fetching: false,
	change_course_navbar_errors: null,
	change_course_navbar_in_progress: false,
});
