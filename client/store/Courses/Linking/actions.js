export default {
	get_linked_courses({ commit }, courseCode) {
		commit("set_linked_courses_fetching", true);
		commit("set_linked_courses_status_code", 0);
		this.$axios
			.$get("/linkedCourse/children/?code=" + courseCode)
			.then(function (res) {
				if (res == null) {
					commit("set_linked_courses", []);
				} else {
					commit("set_linked_courses", res);
				}
				commit("set_linked_courses_fetching", false);
			})
			.catch(function (err) {
				if (err.response) {
					commit("set_linked_courses_status_code", err.response.status);
				} else {
					commit("set_linked_courses_status_code", -1);
				}
				commit("set_linked_courses", null);
				commit("set_linked_courses_fetching", false);
			});
	},

	delete_linked_course({ dispatch, commit, getters }, payload) {
		this.$axios
			.$delete("/linkedCourse/" + payload.parentCode + "/" + payload.code + "/")
			.then(function () {
				let linkedCourses = getters.linked_courses;
				linkedCourses.splice(
					linkedCourses.findIndex(
						(lc) =>
							lc.parentCode === payload.parentCode && lc.code === payload.code
					),
					1
				);
				commit("set_linked_courses", linkedCourses);
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg:
							"Unable to delete " +
							payload.code +
							" from " +
							payload.parentCode +
							" with error: " +
							resError,
					},
					{ root: true }
				);
			});
	},

	add_linked_course({ dispatch }, payload) {
		this.$axios
			.$post("/linkedCourse/", payload)
			.then(function () {
				dispatch("get_linked_courses", payload.parentCode);
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg:
							"Unable to add " +
							payload.code +
							" to " +
							payload.parentCode +
							" with error: " +
							resError,
					},
					{ root: true }
				);
			});
	},
};
