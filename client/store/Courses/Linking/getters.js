export default {
	linked_courses(state) {
		return state.linked_courses;
	},

	linked_courses_fetching(state) {
		return state.linked_courses_fetching;
	},

	linked_courses_status_code(state) {
		return state.linked_courses_status_code;
	},
};
