export default {
	set_linked_courses(state, payload) {
		state.linked_courses = payload;
	},

	set_linked_courses_fetching(state, payload) {
		state.linked_courses_fetching = payload;
	},

	set_linked_courses_status_code(state, payload) {
		state.linked_courses_status_code = payload;
	},
};
