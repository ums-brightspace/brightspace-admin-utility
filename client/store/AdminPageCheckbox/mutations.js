export default {
	set_adminPageCheckboxValue(state, payload) {
		let found = false;
		for (let i = 0; i < state.adminPageCheckboxValues.length; i++) {
			if (state.adminPageCheckboxValues[i].routeName === payload.routeName) {
				found = true;
				state.adminPageCheckboxValues[i].value = payload.value;
				break;
			}
		}

		if (!found) {
			state.adminPageCheckboxValues.push(payload);
		}
	},

	clear_adminPageCheckboxValues(state) {
		state.adminPageCheckboxValues = [];
	},
};
