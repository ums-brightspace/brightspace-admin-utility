export default {
	delete_adminUserPage({ commit, dispatch }, payload) {
		commit("System/AdminUserPermissions/set_adminUserFetching", true, {
			root: true,
		});
		this.$axios
			.$delete(
				"/adminUserPage/" +
					payload.adminUserId +
					"/" +
					payload.adminPageId +
					"/"
			)
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			})
			.finally(function () {
				dispatch(
					"System/AdminUserPermissions/get_adminUser",
					{ id: payload.adminUserId },
					{ root: true }
				);
			});
	},

	add_adminUserPage({ commit, dispatch }, payload) {
		commit("System/AdminUserPermissions/set_adminUserFetching", true, {
			root: true,
		});
		this.$axios
			.$post("/adminUserPage/", payload)
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			})
			.finally(function () {
				dispatch(
					"System/AdminUserPermissions/get_adminUser",
					{ id: payload.adminUserId },
					{ root: true }
				);
			});
	},
};
