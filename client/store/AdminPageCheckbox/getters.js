export default {
	adminPageCheckboxValue(state) {
		return (routeName) => {
			for (let i = 0; i < state.adminPageCheckboxValues.length; i++) {
				if (state.adminPageCheckboxValues[i].routeName === routeName) {
					return state.adminPageCheckboxValues[i].value;
				}
			}

			return false;
		};
	},
};
