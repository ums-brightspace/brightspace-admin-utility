export default {
	get_users_ms({ commit }, payload) {
		commit("set_users_ms", []);
		commit("set_users_ms_fetching", true);

		this.$axios
			.$get("/users/?userId=" + payload.user_id)
			.then(function (res) {
				commit("set_users_ms", res);
				commit("set_users_ms_fetching", false);
			})
			.catch(function () {
				commit("set_users_ms", []);
				commit("set_users_ms_fetching", false);
			});
	},
};
