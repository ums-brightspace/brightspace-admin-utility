export default {
	users_ms(state) {
		return state.users_ms;
	},

	users_ms_fetching(state) {
		return state.users_ms_fetching;
	},

	user_search_id(state) {
		return state.user_search_id;
	},
};
