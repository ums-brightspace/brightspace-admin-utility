module gitlab.its.maine.edu/development/lms/utility

go 1.14

replace gopkg.in/urfave/cli.v1 => github.com/urfave/cli v1.21.0

require (
	github.com/calbis/cas/v2 v2.4.0
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-oci8 v0.1.1
	github.com/onsi/ginkgo v1.15.0
	github.com/onsi/gomega v1.10.5
	github.com/rs/cors v1.7.0
	go.uber.org/zap v1.16.0
	google.golang.org/appengine v1.6.7 // indirect
)
