FROM node:16 as client

ENV CONTEXT_PATH /brightspace-admin-utility

WORKDIR /home/node/app
COPY ./client/.yarn ./.yarn/
COPY client/assets ./assets/
COPY client/components ./components/
COPY client/middleware ./middleware/
COPY client/pages ./pages/
COPY client/static ./static/
COPY client/store ./store/
COPY client/.prettierignore ./
COPY client/.prettierrc.js ./
COPY client/.yarnrc ./
COPY client/nuxt.config.js ./
COPY client/package.json ./
COPY client/yarn.lock ./

RUN yarn --silent --immutable \
  && NODE_ENV=production yarn --silent run generate


FROM registry.gitlab.its.maine.edu/docker/golang/1-15-oracle
WORKDIR /
WORKDIR /go/src/app
COPY server ./server/
COPY .env ./
COPY go.mod ./
COPY go.sum ./
COPY main.go ./

COPY --from=client /home/node/app/dist ./public/

RUN mkdir logs \
    && echo '{"server": "init"}' > logs/log.log

RUN go build -v main.go

COPY splunk.sh ./
RUN chmod +x ./splunk.sh
COPY docker/docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
