package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func University(router *mux.Router) {
	router.Methods("GET").Path("/all/").HandlerFunc(rest.UniversityGetAll)
	router.Methods("PUT").Path("/{id:[0-9]+}/").HandlerFunc(rest.UniversityUpdate)
}
