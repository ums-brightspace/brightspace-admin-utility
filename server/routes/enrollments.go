package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func Enrollments(router *mux.Router) {
	router.Methods("GET").Path("/byCode/").HandlerFunc(rest.GetEnrollmentsByCode)
	router.Methods("GET").Path("/system/").HandlerFunc(rest.GetSystemEnrollments)
	router.Methods("PUT").Path("/system/").HandlerFunc(rest.PutSystemEnrollments)
}
