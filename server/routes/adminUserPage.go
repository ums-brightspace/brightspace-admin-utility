package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func AdminUserPage(router *mux.Router) {
	router.Methods("POST").Path("/").HandlerFunc(rest.AdminUserPageAdd)
	router.Methods("DELETE").Path("/{adminUserId:[0-9]+}/{adminPageId:[0-9]+}/").HandlerFunc(rest.AdminUserPageDelete)
}
