package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func Courses(router *mux.Router) {
	router.Methods("GET").Path("/").HandlerFunc(rest.Courses)
	router.Methods("GET").Path("/by-acad-org/").HandlerFunc(rest.CoursesByAcadOrg)
	router.Methods("GET").Path("/defunct/").HandlerFunc(rest.DefunctCourses)
	router.Methods("GET").Path("/stats/").HandlerFunc(rest.CoursesStats)
	router.Methods("POST").Path("/homepages/").HandlerFunc(rest.ChangeHomepages)
	router.Methods("POST").Path("/navbars/").HandlerFunc(rest.ChangeNavbars)
	router.Methods("POST").Path("/bulk-copy/").HandlerFunc(rest.CoursesBulkCopy)
}
