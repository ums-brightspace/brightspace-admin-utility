package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func Users(router *mux.Router) {
	router.Methods("GET").Path("/").HandlerFunc(rest.UsersGet)
}
