package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func AcademicUnit(router *mux.Router) {
	router.Methods("GET").Path("/set/").HandlerFunc(rest.AcademicUnitGetSet)
	router.Methods("GET").Path("/all/").HandlerFunc(rest.AcademicUnitGetAll)
	router.Methods("PUT").Path("/{id:[0-9]+}/").HandlerFunc(rest.AcademicUnitUpdate)
}
