package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func TermsToProcess(router *mux.Router) {
	router.Methods("GET").Path("/").HandlerFunc(rest.TermsToProcessGet)
	router.Methods("POST").Path("/").HandlerFunc(rest.TermsToProcessAdd)
	router.Methods("DELETE").Path("/{campus}/{term}").HandlerFunc(rest.TermsToProcessDelete)
}
