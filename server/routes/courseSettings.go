package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func CourseSettings(router *mux.Router) {
	router.Methods("GET").Path("/{campus}/").HandlerFunc(rest.CourseSettingsGet)
	router.Methods("PUT").Path("/{campus}/").HandlerFunc(rest.CourseSettingsUpdate)
}
