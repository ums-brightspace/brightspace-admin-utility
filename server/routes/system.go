package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func System(router *mux.Router) {
	router.Methods("GET").Path("/authedUser/").HandlerFunc(rest.AuthedUser)
	router.Methods("GET").Path("/environment/").HandlerFunc(rest.Environment)
}
