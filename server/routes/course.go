package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func Course(router *mux.Router) {
	router.Methods("GET").Path("/byCode/").HandlerFunc(rest.GetCourseByCode)
}
