package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func LmsUser(router *mux.Router) {
	router.Methods("GET").Path("/byEmplid/").HandlerFunc(rest.GetLmsUserByEmplid)
	router.Methods("PUT").Path("/byEmplid/").HandlerFunc(rest.PutLmsUserByEmplid)
}
