package rest

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"regexp"
)

type CourseBulkCopy struct {
	Institution   string      `json:"institution"`
	Term          string      `json:"term"`
	AcadOrg       string      `json:"acadOrg"`
	CoursesToCopy []db.Course `json:"coursesToCopy"`
}

func CourseBulkCopyValidate(cbc CourseBulkCopy) []error {
	var errs []error

	if err := db.ValidateInstitution(cbc.Institution); err != nil {
		errs = append(errs, err)
	}

	if cbc.Institution != "UMS07" {
		errs = append(errs, fmt.Errorf("institution is not allowed"))
	}

	if err := db.ValidateTerm(cbc.Term); err != nil {
		errs = append(errs, err)
	}

	if cbc.AcadOrg != "I-DIST" {
		errs = append(errs, fmt.Errorf("academic organization is not allowed"))
	}

	if len(cbc.CoursesToCopy) <= 0 {
		errs = append(errs, fmt.Errorf("must include at least 1 course to copy"))
	}

	re := regexp.MustCompile(`^[0-9]{0,7}$`)
	for _, ctc := range cbc.CoursesToCopy {
		if !re.MatchString(ctc.LmsId) {
			errs = append(errs, fmt.Errorf("course, %s, is invalid", ctc.LmsId))
		}
	}

	return errs
}
