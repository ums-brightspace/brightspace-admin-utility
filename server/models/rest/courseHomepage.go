package rest

type CourseHomepage struct {
	Institution string `json:"institution" db:"INSTITUTION"`
	Term        string `json:"term" db:"TERM"`
	Homepage    string `json:"homepage"`
}
