package rest

import (
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
)

type RoleOverride struct {
	UserLmsId string `json:"userLmsId"`
	Username  string `json:"username"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Emplid    string `json:"emplid"`
	RoleInd   string `json:"roleInd"`
	RoleId    int    `json:"roleId"`
	RoleName  string `json:"roleName"`
}

func GetAllRoleOverrides() ([]RoleOverride, error) {
	helpers.Logger().Debugw("Gets all the role overrides in the system",
		"package", "models.rest",
		"method", "GetAllRoleOverrides")
	dbRoleOverrides, err := db.GetAllRoleOverrides()
	if err != nil {
		return []RoleOverride{}, err
	}

	lmsRoles, err := lms.GetAllRoles()
	if err != nil {
		return []RoleOverride{}, err
	}

	roleOverrides := make([]RoleOverride, len(dbRoleOverrides))
	for i, value := range dbRoleOverrides {
		roleOverrides[i].UserLmsId = value.UserLmsId
		roleOverrides[i].Username = value.Username
		roleOverrides[i].Emplid = value.Emplid
		roleOverrides[i].FirstName = value.FirstName
		roleOverrides[i].LastName = value.LastName
		roleOverrides[i].RoleInd = value.RoleInd
		roleOverrides[i].RoleId = lms.GetRoleIdByInd(lmsRoles, value.RoleInd)
		roleOverrides[i].RoleName = lms.GetRoleNameByInd(lmsRoles, value.RoleInd)
	}

	return roleOverrides, nil
}
