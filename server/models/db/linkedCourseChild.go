package db

type LinkedCourseChild struct {
	ParentCode      string `json:"parentCode" db:"PARENT_CODE"`
	Institution     string `json:"institution" db:"INSTITUTION"`
	Term            string `json:"term" db:"TERM"`
	ClassNumber     string `json:"classNumber" db:"CLASS_NUMBER"`
	IsCombined      string `json:"isCombined" db:"IS_COMBINED"`
	SessionCode     string `json:"sessionCode" db:"SESSION_CODE"`
	Title           string `json:"title" db:"TITLE"`
	Code            string `json:"code" db:"CODE"`
	InstructorNames string `json:"instructorNames" db:"INSTRUCTOR_NAMES"`
	LmsId           string `json:"lmsId" db:"LMS_ID"`
}
