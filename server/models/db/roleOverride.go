package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type RoleOverride struct {
	UserLmsId string `json:"userLmsId" db:"USER_LMS_ID"`
	RoleInd   string `json:"role" db:"ROLE_IND"`
	Username  string `json:"username" db:"USERNAME"`
	FirstName string `json:"firstName" db:"FIRST_NAME"`
	LastName  string `json:"lastName" db:"LAST_NAME"`
	Emplid    string `json:"emplid" db:"EMPLID"`
}

func GetAllRoleOverrides() ([]RoleOverride, error) {
	helpers.Logger().Debugw("Getting all role overrides for all users",
		"package", "models.db",
		"method", "GetAllRoleOverrides")
	dbConn := helpers.DbConn()
	var roleOverrides []RoleOverride
	query := "Select USER_LMS_ID, ROLE_IND, USERNAME, EMPLID, FIRST_NAME, LAST_NAME From ADMIN_ROLE_OVERRIDES"
	err := dbConn.Select(&roleOverrides, query)
	if err != nil {
		return []RoleOverride{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	return roleOverrides, nil
}

func DeleteRoleOverride(userLmsId string) error {
	helpers.Logger().Debugw("Deleting a user's role override",
		"package", "models.db",
		"method", "DeleteRoleOverride", "userLmsId", userLmsId)
	_, err := helpers.DbConn().Exec("Delete From ROLE_OVERRIDE Where USER_LMS_ID=:USER_LMS_ID", userLmsId)
	if err != nil {
		return fmt.Errorf("error removing role override with error: %+v", err)
	}

	return nil
}

func AddRoleOverride(roleOverride RoleOverride) error {
	helpers.Logger().Debugw("Add a role override",
		"package", "models.db",
		"method", "AddRoleOverride", "roleOverride", roleOverride)
	_, err := helpers.DbConn().NamedExec("Insert Into ROLE_OVERRIDE (USER_LMS_ID, ROLE_IND) Values (:USER_LMS_ID, :ROLE_IND)", &roleOverride)
	if err != nil {
		return fmt.Errorf("error adding role override with error: %+v", err)
	}

	return nil
}

func DoesUserHaveAnOveriddenRole(userLmsId string) (bool, error) {
	helpers.Logger().Debugw("Checking if user has a role override",
		"package", "models.db",
		"method", "DoesUserHaveAnOveriddenRole", "userLmsId", userLmsId)
	dbConn := helpers.DbConn()
	row, err := dbConn.Query("Select count(*) ROWCOUNT From ROLE_OVERRIDE Where USER_LMS_ID=:USER_LMS_ID", userLmsId)
	if err != nil {
		return false, fmt.Errorf("error checking if user already has a role override with error: %+v", err)
	}

	var count int
	if !row.Next() {
		return false, fmt.Errorf("error getting count of user role overrides with error: %+v", err)
	}

	err = row.Scan(&count)
	if err != nil {
		return false, fmt.Errorf("error counting user role overrides with error: %+v", err)
	}

	return count > 0, nil
}
