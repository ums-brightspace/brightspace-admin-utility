package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type AdminUserPermission struct {
	Id                int64 `json:"id,omitempty" db:"ID"`
	AdminUserId       int64 `json:"adminUserId,omitempty" db:"ADMIN_USER_ID"`
	AdminPermissionId int64 `json:"adminPermissionId,omitempty" db:"ADMIN_PERMISSION_ID"`
}

func AdminUserPermissionAdd(adminUserPermission AdminUserPermission) error {

	sqlResult, err := helpers.DbConn().NamedExec("Insert Into ADMIN_USER_PERMISSION (ADMIN_USER_ID, ADMIN_PERMISSION_ID) Values (:ADMIN_USER_ID, :ADMIN_PERMISSION_ID)", &adminUserPermission)
	if err != nil {
		return fmt.Errorf("error adding admin user permission with error: %+v", err)
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return fmt.Errorf("add to ADMIN_USER_PERMISSION error %d rows affected with error: %+v", rowsAffected, err.Error())
	}

	if rowsAffected == 1 {
		return nil
	}

	return fmt.Errorf("add to ADMIN_USER_PERMISSION unexpected %d rows affected with error: %+v", rowsAffected, err)
}

func AdminUserPermissionDelete(adminUserPermission AdminUserPermission) error {
	sqlResult, err := helpers.DbConn().NamedExec("Delete From ADMIN_USER_PERMISSION Where ADMIN_USER_ID=:ADMIN_USER_ID And ADMIN_PERMISSION_ID=:ADMIN_PERMISSION_ID", &adminUserPermission)
	if err != nil {
		return fmt.Errorf("error removing admin user permission with error: %+v", err)
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return fmt.Errorf("delete from ADMIN_USER_PERMISSION error %d rows affected with error: %+v", rowsAffected, err.Error())
	}

	if rowsAffected != 1 {
		helpers.Logger().Warnw("Deleting from ADMIN_USER_PERMISSION resulted in unexpected rows affected", "AdminUserPermission", adminUserPermission, "RowsAffected", rowsAffected)
	}

	return nil
}

func DoesAdminUserPermissionExist(adminUserPermission AdminUserPermission) (bool, error) {
	dbConn := helpers.DbConn()
	row, err := dbConn.NamedQuery("Select count(*) ROWCOUNT From ADMIN_USER_PERMISSION Where ADMIN_USER_ID=:ADMIN_USER_ID And ADMIN_PERMISSION_ID=:ADMIN_PERMISSION_ID", &adminUserPermission)
	if err != nil {
		return false, fmt.Errorf("error checking if user permission already exists with error: %+v", err)
	}

	var count int
	if !row.Next() {
		return false, fmt.Errorf("error getting count of user permissions with error: %+v", err)
	}

	err = row.Scan(&count)
	if err != nil {
		return false, fmt.Errorf("error counting user permission adding admin user with error: %+v", err)
	}

	return count > 0, nil
}

func DeleteAdminUserPermissionsByPage(page AdminUserPage) error {
	statement := `
Delete
From ADMIN_USER_PERMISSION
Where ADMIN_USER_ID = :ADMIN_USER_ID
  and ADMIN_PERMISSION_ID in (
    Select APP.ADMIN_PERMISSION_ID
    From ADMIN_PAGE_PERMISSION APP
    Inner Join ADMIN_USER_PERMISSION AUP
               on APP.ADMIN_PERMISSION_ID = AUP.ADMIN_PERMISSION_ID
                   And APP.ADMIN_PAGE_ID = :ADMIN_PAGE_ID
                   And AUP.ADMIN_USER_ID = :ADMIN_USER_ID
    MINUS
    Select APP.ADMIN_PERMISSION_ID
    From ADMIN_PAGE_PERMISSION APP
    Inner Join ADMIN_USER_PAGE AUP
               on APP.ADMIN_PAGE_ID = AUP.ADMIN_PAGE_ID
                   And AUP.ADMIN_USER_ID = :ADMIN_USER_ID
)
`
	_, err := helpers.DbConn().NamedExec(statement, &page)
	if err != nil {
		return fmt.Errorf("error removing admin user permissions by page with error: %+v", err)
	}

	return nil
}

func AddAdminUserPermissionsByPage(page AdminUserPage) error {
	statement := `
Insert Into ADMIN_USER_PERMISSION
(ADMIN_USER_ID, ADMIN_PERMISSION_ID)
Select :ADMIN_USER_ID, AP.ID
From ADMIN_PERMISSION AP
Inner Join ADMIN_PAGE_PERMISSION APP
    on AP.ID = APP.ADMIN_PERMISSION_ID
    And APP.ADMIN_PAGE_ID = :ADMIN_PAGE_ID
And AP.ID Not in (
    Select AUP.ADMIN_PERMISSION_ID
    From ADMIN_USER_PERMISSION AUP
    Where ADMIN_USER_ID = :ADMIN_USER_ID
            )
`
	_, err := helpers.DbConn().NamedExec(statement, &page)
	if err != nil {
		return fmt.Errorf("error adding admin user permissions by page with error: %+v", err)
	}

	return nil
}
