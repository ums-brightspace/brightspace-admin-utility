package db

type TermsToProcess struct {
	Institution     string `json:"institution" db:"INSTITUTION"`
	Term            string `json:"term" db:"TERM"`
	CampusName      string `json:"CampusName" db:"CAMPUS_NAME"`
	CampusShortName string `json:"campusShortName" db:"CAMPUS_SHORT_NAME"`
	TermName        string `json:"termName" db:"TERM_NAME"`
	PK              string `json:"pk" db:"PK"`
}
