package db

type CourseStats struct {
	QueuedCourses   int `json:"queuedCourses" db:"QUEUED_COURSES"`
	ProcessQuantity int `json:"processQuantity" db:"PROCESS_QUANTITY"`
}
