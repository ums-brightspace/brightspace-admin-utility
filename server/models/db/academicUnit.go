package db

type AcademicUnit struct {
	Code                string `json:"code" db:"CODE"`
	Name                string `json:"name" db:"NAME"`
	Parent              string `json:"parent" db:"PARENT"`
	LmsId               string `json:"lms_id" db:"LMS_ID"`
	TemplateLmsIdFall   string `json:"template_lms_id_fall" db:"TEMPLATE_LMS_ID_FALL"`
	TemplateLmsIdSpring string `json:"template_lms_id_spring" db:"TEMPLATE_LMS_ID_SPRING"`
	TemplateLmsIdSummer string `json:"template_lms_id_summer" db:"TEMPLATE_LMS_ID_SUMMER"`
}
