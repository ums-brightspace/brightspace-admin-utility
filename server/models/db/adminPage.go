package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type AdminPage struct {
	Id               int64             `json:"id,omitempty" db:"ID"`
	RouteName        string            `json:"routeName,omitempty" db:"ROUTE_NAME"`
	Description      string            `json:"description,omitempty" db:"DESCRIPTION"`
	IsActive         bool              `json:"isActive,omitempty" db:"IS_ACTIVE"`
	AdminPermissions []AdminPermission `json:"adminPermissions,omitempty"`
}

func GetAdminPage(adminPageId int64) (AdminPage, error) {
	dbConn := helpers.DbConn()
	var pages []AdminPage
	query := "Select ID, ROUTE_NAME, DESCRIPTION, IS_ACTIVE From ADMIN_PAGE Where ID=:ID"
	err := dbConn.Select(&pages, query, adminPageId)
	if err != nil {
		return AdminPage{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	return pages[0], nil
}

func GetAllowedPages(adminUser AdminUser) ([]AdminPage, error) {
	dbConn := helpers.DbConn()
	var pages []AdminPage
	query := "Select AP.ID, AP.ROUTE_NAME, AP.DESCRIPTION, AP.IS_ACTIVE From ADMIN_USER_PAGE AUP Inner Join ADMIN_PAGE AP On AUP.ADMIN_PAGE_ID = AP.ID And AP.IS_ACTIVE=1 Inner Join ADMIN_USER AU On AUP.ADMIN_USER_ID=AU.ID And AU.IS_ACTIVE=1 Where AUP.ADMIN_USER_ID=:ID"
	err := dbConn.Select(&pages, query, adminUser.Id)
	if err != nil {
		return []AdminPage{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	if len(pages) == 0 {
		return []AdminPage{}, nil
	}

	return pages, nil
}

func GetActiveAdminPages() ([]AdminPage, error) {
	dbConn := helpers.DbConn()
	var pages []AdminPage
	query := "Select ID, ROUTE_NAME, DESCRIPTION, IS_ACTIVE From ADMIN_PAGE Where IS_ACTIVE=1"
	err := dbConn.Select(&pages, query)
	if err != nil {
		return []AdminPage{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	return pages, nil
}

func GetActiveAdminPagesWithPermissions() ([]AdminPage, error) {
	pages, err := GetActiveAdminPages()
	if err != nil {
		return []AdminPage{}, nil
	}

	for p, page := range pages {
		perms, err := GetActiveAdminPermissionsByAdminPage(page.Id)
		if err != nil {
			helpers.Logger().Debugf("error getting permissions for page, %v, with error: %v", page.Id, err)
		}
		if err == nil {
			pages[p].AdminPermissions = perms
		}
	}

	return pages, nil
}
