package db

import (
	"database/sql"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"time"
)

type CourseCompleted struct {
	Code        string    `json:"code" db:"CODE"`
	Title       string    `json:"title" db:"TITLE"`
	Parents     string    `json:"parents" db:"PARENTS"`
	Master      string    `json:"master" db:"MASTER"`
	Semester    string    `json:"semester" db:"SEMESTER"`
	ClassNumber string    `json:"classNumber" db:"CLASS_NUMBER"`
	Institution string    `json:"institution" db:"INSTITUTION"`
	Term        string    `json:"term" db:"TERM"`
	IsCombined  string    `json:"isCombined" db:"IS_COMBINED"`
	SessionCode string    `json:"sessionCode" db:"SESSION_CODE"`
	AddedOn     time.Time `json:"addedOn" db:"ADDED_ON"`
	ModifiedOn  time.Time `json:"modifiedOn" db:"MODIFIED_ON"`
	LmsId       string    `json:"lmsId" db:"LMS_ID"`
}

func GetAllByCampusAndTerm(institution string, term string) (courses []CourseCompleted, err error) {
	dbConn := helpers.DbConn()

	query := fmt.Sprintf("Select LMS_ID From COURSE_COMPLETED Where TERM = '%s' And INSTITUTION = '%s'", term, institution)

	results, err := dbConn.Queryx(query)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			courses = nil
			return
		}
		err = fmt.Errorf("query, <<<%s>>>, failed:%v", query, err)
		return
	}
	defer results.Close()

	var c CourseCompleted
	for results.Next() {
		err := results.StructScan(&c)
		if err != nil {
			err = fmt.Errorf("unable to read results with error, %v", err)
			break
		}

		courses = append(courses, c)
	}

	return
}

func IsCourseCompletedExist(code string) (isExist bool, err error) {
	dbConn := helpers.DbConn()
	query := "Select CODE From COURSE_COMPLETED  Where CODE = :CODE"
	err = dbConn.Get(&code, query, code)
	isExist = true
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		} else {
			err = fmt.Errorf("query, <<<%s>>>, failed: %+v", query, err)
			return
		}
	}
	return
}
func InsertCourseCompleted(courseCompleted CourseCompleted) (err error) {
	dbConn := helpers.DbConn()
	query := "Insert Into COURSE_COMPLETED (CODE, TITLE, PARENTS, MASTER, SEMESTER, CLASS_NUMBER, INSTITUTION, TERM, IS_COMBINED, SESSION_CODE, LMS_ID, ADDED_ON, MODIFIED_ON) Values (:CODE, :TITLE, :PARENTS, :MASTER, :SEMESTER, :CLASS_NUMBER, :INSTITUTION, :TERM, :IS_COMBINED, :SESSION_CODE, :LMS_ID, :ADDED_ON, :MODIFIED_ON)"
	sqlResult, err := dbConn.NamedExec(query, &courseCompleted)
	if err != nil {
		err = fmt.Errorf("error adding course To COURSE_COMPLETED on terms: %+v, with error: %+v", &courseCompleted, err)
		return
	}
	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("error adding course To COURSE_COMPLETED on terms: %+v, with error: %+v", &courseCompleted, err)
		return
	}

	if rowsAffected != 1 {
		err = fmt.Errorf("error adding course To COURSE_COMPLETED on terms: %+v, with error: %+v", &courseCompleted, err)
		return
	}
	return
}
func UpdateCourseCompleted(courseCompleted CourseCompleted) (err error) {
	var query = "Update COURSE_COMPLETED Set TITLE = :TITLE, PARENTS = :PARENTS, MASTER = :MASTER, SEMESTER = :SEMESTER, CLASS_NUMBER = :CLASS_NUMBER, INSTITUTION = :INSTITUTION, TERM = :TERM, IS_COMBINED = :IS_COMBINED, SESSION_CODE = :SESSION_CODE, MODIFIED_ON = :MODIFIED_ON, LMS_ID = :LMS_ID Where CODE = :CODE"
	sqlResult, err := helpers.DbConn().NamedExec(query, &courseCompleted)
	if err != nil {
		err = fmt.Errorf("error updating course To COURSE_COMPLETED on terms: %+v, with error: %+v", &courseCompleted, err)
		return
	}
	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("error updating course To COURSE_COMPLETED on terms: %+v, with error: %+v", &courseCompleted, err)
		return
	}

	if rowsAffected != 1 {
		err = fmt.Errorf("error adding course To COURSE_COMPLETED on terms: %+v, with error: %+v", &courseCompleted, err)
		return
	}
	return
}
func DeleteCourseCompleted(code string) (err error) {
	cc := CourseCompleted{
		Code: code,
	}
	dbConn := helpers.DbConn()
	sqlResult, err := dbConn.NamedExec("Delete From COURSE_COMPLETED Where CODE = :CODE", &cc)

	if err != nil {
		err = fmt.Errorf("error deleting course from COURSE_COMPLETED on terms: %+v, with error: %+v", &code, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("delete from COURSE_COMPLETED error %d rows affected for course: %+v, and error: %+v", rowsAffected, code, err)
		return
	}

	if rowsAffected <= 1 {
		return
	}
	err = fmt.Errorf("delete from COURSE_COMPLETED unexpected %d rows affected for course: %+v, and error: %+v", rowsAffected, code, err)
	return
}
