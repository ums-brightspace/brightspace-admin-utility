package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type Enrollment struct {
	Institution  string `json:"institution" db:"INSTITUTION"`
	Term         string `json:"term" db:"TERM"`
	ClassNumber  string `json:"classNumber" db:"CLASS_NUMBER"`
	IsCombined   string `json:"isCombined" db:"IS_COMBINED"`
	SessionCode  string `json:"sessionCode" db:"SESSION_CODE"`
	Type         string `json:"type" db:"TYPE"`
	Emplid       string `json:"emplid" db:"EMPLID"`
	FirstName    string `json:"firstName" db:"FIRST_NAME"`
	LastName     string `json:"lastName" db:"LAST_NAME"`
	EnrollStatus string `json:"enrollStatus" db:"ENROLL_STATUS"`
	UserLmsId    string `json:"userLmsId" db:"USER_LMS_ID"`
	CourseLmsId  string `json:"courseLmsId" db:"COURSE_LMS_ID"`
}

func GetUserEnrollmentByClass(emplid string, course Course) (enrollment Enrollment, err error) {
	dbConn := helpers.DbConn()
	query := "Select INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TYPE, EMPLID, FIRST_NAME, LAST_NAME, ENROLL_STATUS, USER_LMS_ID, COURSE_LMS_ID From LMS_ENROLLMENTS_BY_CLASS Where EMPLID = :EMPLID And INSTITUTION = :INSTITUTION And TERM = :TERM And CLASS_NUMBER = :CLASS_NUMBER And IS_COMBINED = :IS_COMBINED And SESSION_CODE = :SESSION_CODE"
	err = dbConn.Get(&enrollment, query, emplid, course.Institution, course.Term, course.ClassNumber, course.IsCombined, course.SessionCode)
	if err != nil {
		err = fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
		return
	}

	return
}
