package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type User struct {
	Username   string `json:"username" db:"USERNAME"`
	FirstName  string `json:"firstName" db:"FIRST_NAME"`
	LastName   string `json:"lastName" db:"LAST_NAME"`
	Emplid     string `json:"emplid" db:"EMPLID"`
	Email      string `json:"email" db:"EMAIL"`
	MiddleName string `json:"middleName" db:"MIDDLE_NAME"`
	LmsId      string `json:"lmsId" db:"LMS_ID"`
}

func GetUserLmsId(username string) (string, error) {
	dbConn := helpers.DbConn()
	var users []User
	query := "Select USERNAME, FIRST_NAME, LAST_NAME, EMPLID, EMAIL, MIDDLE_NAME, LMS_ID From USER_COMPLETED Where USERNAME = :USERNAME"
	err := dbConn.Select(&users, query, username)
	if err != nil {
		return "", fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	if len(users) == 0 {
		return "", fmt.Errorf("user, %s, not found", username)
	}

	return users[0].LmsId, nil
}

func GetUserLmsIdByEmplid(emplid string) (string, error) {
	dbConn := helpers.DbConn()
	var users []User
	query := "Select USERNAME, FIRST_NAME, LAST_NAME, EMPLID, EMAIL, MIDDLE_NAME, LMS_ID From USER_COMPLETED Where EMPLID = :EMPLID"
	err := dbConn.Select(&users, query, emplid)
	if err != nil {
		return "", fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	if len(users) == 0 {
		return "", fmt.Errorf("user, %s, not found", emplid)
	}

	return users[0].LmsId, nil
}
