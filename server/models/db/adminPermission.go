package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type AdminPermission struct {
	Id          int64  `json:"id,omitempty" db:"ID"`
	Category    string `json:"category,omitempty" db:"CATEGORY"`
	Route       string `json:"route,omitempty" db:"ROUTE"`
	Action      string `json:"action,omitempty" db:"ACTION"`
	Description string `json:"description,omitempty" db:"DESCRIPTION"`
	IsActive    bool   `json:"isActive,omitempty" db:"IS_ACTIVE"`
	Name        string `json:"name,omitempty" db:"NAME"`
}

func GetAllowedPermissions(adminUser AdminUser) ([]AdminPermission, error) {
	dbConn := helpers.DbConn()
	var perms []AdminPermission
	query := "Select AP.DESCRIPTION, AP.CATEGORY, AP.ACTION, AP.ROUTE, AP.ID, AP.IS_ACTIVE, AP.NAME From ADMIN_USER_PERMISSION AUP Inner Join ADMIN_PERMISSION AP On AUP.ADMIN_PERMISSION_ID = AP.ID And AP.IS_ACTIVE=1 Inner Join ADMIN_USER AU On AUP.ADMIN_USER_ID=AU.ID And AU.IS_ACTIVE=1 Where AUP.ADMIN_USER_ID=:ID"
	err := dbConn.Select(&perms, query, adminUser.Id)
	if err != nil {
		return []AdminPermission{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	if len(perms) == 0 {
		return []AdminPermission{}, nil
	}

	return perms, nil
}

func GetActiveAdminPermissions() ([]AdminPermission, error) {
	dbConn := helpers.DbConn()
	var perms []AdminPermission
	query := "Select ID, CATEGORY, ROUTE, ACTION, DESCRIPTION, IS_ACTIVE, NAME From ADMIN_PERMISSION Where IS_ACTIVE=1"
	err := dbConn.Select(&perms, query)
	if err != nil {
		return perms, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	return perms, nil
}

func GetActiveAdminPermissionsByAdminPage(adminPageId int64) ([]AdminPermission, error) {
	dbConn := helpers.DbConn()
	var perms []AdminPermission
	query := "Select AP.ID, AP.CATEGORY, AP.ROUTE, AP.ACTION, AP.DESCRIPTION, AP.IS_ACTIVE, AP.NAME From ADMIN_PERMISSION AP Inner Join ADMIN_PAGE_PERMISSION APP on AP.ID = APP.ADMIN_PERMISSION_ID And AP.IS_ACTIVE=1 And APP.ADMIN_PAGE_ID=:ADMIN_PAGE_ID"
	err := dbConn.Select(&perms, query, adminPageId)
	if err != nil {
		return perms, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	return perms, nil
}

func GetAdminPermission(id int64) (AdminPermission, error) {
	dbConn := helpers.DbConn()
	var perms []AdminPermission
	query := "Select AP.DESCRIPTION, AP.CATEGORY, AP.ACTION, AP.ROUTE, AP.ID, AP.IS_ACTIVE, AP.NAME From ADMIN_PERMISSION AP Where ID=:ID"
	err := dbConn.Select(&perms, query, id)
	if err != nil {
		return AdminPermission{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	if len(perms) == 0 {
		return AdminPermission{}, fmt.Errorf("permission not found")
	}

	return perms[0], nil
}
