package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type Semester struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

func CreateSemester(semester Semester) (lmsId string, err error) {
	payload, err := json.Marshal(semester)
	if err != nil {
		return
	}

	data, statusCode := helpers.HttpRequest("POST", "/semester", bytes.NewBuffer(payload))
	if statusCode >= 400 {
		err = fmt.Errorf("failure when creating the semester, %s", semester.Code)
		return
	}

	semesterId := Id{}
	err = json.Unmarshal(data, &semesterId)
	if err != nil {
		err = fmt.Errorf("unable to read response for POST /semester with data: %s", semester.Code)
		return
	}

	lmsId = semesterId.Id

	return
}
