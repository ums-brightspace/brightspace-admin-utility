package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type CreateCourseStruct struct {
	Name     string   `json:"name"`
	Code     string   `json:"code"`
	Semester string   `json:"semester"`
	Parents  []string `json:"parents"`
	Master   string   `json:"master"`
}

func CreateCourse(createCourse CreateCourseStruct) (course Course, err error) {
	course = Course{}
	payload, err := json.Marshal(createCourse)
	if err != nil {
		return
	}

	data, statusCode := helpers.HttpRequest("POST", "/course", bytes.NewBuffer(payload))
	if statusCode >= 400 {
		err = fmt.Errorf("%d failure when creating course %s: %+v", statusCode, createCourse.Code, data)
		return
	}

	id := Id{}
	err = json.Unmarshal(data, &id)
	if err != nil {
		err = fmt.Errorf("unable to read response for POST /course, %s, with data: %s", createCourse.Code, string(data))
		return
	}

	course.Code = createCourse.Code
	course.Identifier = id.Id
	course.Name = createCourse.Name

	return
}
