package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type Course struct {
	Identifier string
	Name       string
	Code       string
	IsActive   bool
	Path       string
	StartDate  string
	EndDate    string
}

func GetCourse(courseLmsId int) (course Course, err error) {
	data, statusCode := helpers.HttpRequest("GET", "/course/"+strconv.Itoa(courseLmsId), nil)
	if statusCode >= 400 {
		course = Course{}
		err = fmt.Errorf("failure when requesting course info for %d", courseLmsId)
		return
	}

	err = json.Unmarshal(data, &course)
	if err != nil {
		course = Course{}
		err = fmt.Errorf("unable to read response for GET /course/%d with data: %s", courseLmsId, string(data))
		return
	}

	return
}
func DeleteCourse(courseLmsId int) (err error) {
	_, statusCode := helpers.HttpRequest("DELETE", "/course/"+strconv.Itoa(courseLmsId), nil)
	if statusCode >= 400 {
		err = fmt.Errorf("%d  failure when deleting course for %d", statusCode, courseLmsId)
		return err
	}
	return
}

func CourseCopy(courseLmsId string, masterCourseCode string) error {
	payload, err := json.Marshal(map[string]string{"masterCourseCode": masterCourseCode})
	if err != nil {
		return err
	}

	_, statusCode := helpers.HttpRequest("POST", "/course/copy-master/"+courseLmsId, bytes.NewBuffer(payload))
	if statusCode >= 400 {
		return fmt.Errorf("failure when copying %s into %s", masterCourseCode, courseLmsId)
	}

	return nil
}
