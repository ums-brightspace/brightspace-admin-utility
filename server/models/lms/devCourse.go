package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type CreateDevCourseStruct struct {
	Name     string `json:"name"`
	Code     string `json:"code"`
	Master   string `json:"master,omitempty"`
	IsMaster bool   `json:"isMaster"`
}

func CreateDevCourse(c CreateDevCourseStruct) (course Course, err error, statusCode int) {
	course = Course{}
	payload, err := json.Marshal(c)
	if err != nil {
		return
	}

	data, statusCode := helpers.HttpRequest("POST", "/course/dev", bytes.NewBuffer(payload))
	if statusCode == 409 {
		err = fmt.Errorf("course, %s, already exists", c.Code)
		return
	} else if statusCode >= 400 {
		err = fmt.Errorf("%d failure when creating course %s: %+v", statusCode, course.Code, data)
		return
	}
	id := Id{}
	err = json.Unmarshal(data, &id)
	if err != nil {
		err = fmt.Errorf("unable to read response for POST /course/dev, %s, with data: %s", course.Code, string(data))
		return
	}
	course.Identifier = id.Id
	course.Code = c.Code
	course.Name = c.Name

	return
}
