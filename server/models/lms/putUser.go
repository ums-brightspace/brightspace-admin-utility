package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type PutUser struct {
	FirstName     string   `json:"firstName"`
	MiddleName    string   `json:"middleName"`
	LastName      string   `json:"lastName"`
	UserName      string   `json:"username"`
	ExternalEmail string   `json:"email"`
	OrgDefinedId  string   `json:"userId"`
	PrimaryCampus string   `json:"primaryCampus"`
	Campuses      []string `json:"campuses"`
}

func UpdateUser(userLmsId int, user PutUser) (resUser PutUser, err error) {
	payload, err := json.Marshal(user)
	if err != nil {
		resUser = PutUser{}
		return
	}

	data, statusCode := helpers.HttpRequest("PUT", "/user/"+strconv.Itoa(userLmsId), bytes.NewBuffer(payload))
	if statusCode >= 400 {
		resUser = PutUser{}
		err = fmt.Errorf("failure when updating user for %d", userLmsId)
		return
	}

	err = json.Unmarshal(data, &resUser)
	if err != nil {
		resUser = PutUser{}
		err = fmt.Errorf("unable to read response for PUT /user/%d with data: %s", userLmsId, string(data))
		return
	}

	return
}
