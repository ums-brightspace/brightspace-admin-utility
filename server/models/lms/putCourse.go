package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type PutCourse struct {
	Identifier string `json:"code"`
	Name       string `json:"name"`
}

func UpdateCourse(courseLmsId int, course PutCourse) (resCourse PutCourse, err error) {
	payload, err := json.Marshal(course)
	if err != nil {
		resCourse = PutCourse{}
		return
	}

	data, statusCode := helpers.HttpRequest("PUT", "/course/"+strconv.Itoa(courseLmsId), bytes.NewBuffer(payload))
	if statusCode >= 400 {
		resCourse = PutCourse{}
		err = fmt.Errorf("failure when updating course for %d", courseLmsId)
		return
	}

	err = json.Unmarshal(data, &resCourse)
	if err != nil {
		resCourse = PutCourse{}
		err = fmt.Errorf("unable to read response for PUT /course/%d with data: %s", courseLmsId, string(data))
		return
	}

	return
}
