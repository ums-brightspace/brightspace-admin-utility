package server

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers"
	"gitlab.its.maine.edu/development/lms/utility/server/middleware"
	"gitlab.its.maine.edu/development/lms/utility/server/routes"
)

func NewRouter(apiPrefix string, auth middleware.Auth) *mux.Router {
	router := mux.NewRouter()

	restRouter := router.PathPrefix(apiPrefix).Subrouter()
	restRouter.Use(auth.AuthHandler)

	routes.System(restRouter.PathPrefix("/system").Subrouter())
	routes.TermsToProcess(restRouter.PathPrefix("/termsToProcess").Subrouter())
	routes.LmsCourse(restRouter.PathPrefix("/lmsCourse").Subrouter())
	routes.Courses(restRouter.PathPrefix("/courses").Subrouter())
	routes.Course(restRouter.PathPrefix("/course").Subrouter())
	routes.University(restRouter.PathPrefix("/university").Subrouter())
	routes.College(restRouter.PathPrefix("/college").Subrouter())
	routes.AcademicUnit(restRouter.PathPrefix("/academicUnit").Subrouter())
	routes.Enrollments(restRouter.PathPrefix("/enrollments").Subrouter())
	routes.LmsEnrollments(restRouter.PathPrefix("/lmsEnrollments").Subrouter())
	routes.LmsEnrollment(restRouter.PathPrefix("/lmsEnrollment").Subrouter())
	routes.LinkedCourse(restRouter.PathPrefix("/linkedCourse").Subrouter())
	routes.Users(restRouter.PathPrefix("/users").Subrouter())
	routes.LmsUser(restRouter.PathPrefix("/lmsUser").Subrouter())
	routes.CourseSettings(restRouter.PathPrefix("/courseSettings").Subrouter())
	routes.AdminUser(restRouter.PathPrefix("/adminUser").Subrouter())
	routes.AdminUserPermission(restRouter.PathPrefix("/adminUserPermission").Subrouter())
	routes.AdminPage(restRouter.PathPrefix("/adminPage").Subrouter())
	routes.AdminUserPage(restRouter.PathPrefix("/adminUserPage").Subrouter())
	routes.RoleOverride(restRouter.PathPrefix("/roleOverride").Subrouter())

	vue := handlers.VueHandler{StaticPath: "public", IndexPath: "index.html"}
	router.PathPrefix("/").Handler(vue)

	return router
}
