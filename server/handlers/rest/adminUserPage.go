package rest

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"strconv"
)

func AdminUserPageDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	adminUserId, err := strconv.ParseInt(vars["adminUserId"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	isSame, _ := db.IsUserSame(adminUserId, helpers.GetLoggedInUser(r))
	if isSame {
		helpers.HttpRespond403(w)
		return
	}

	adminPageId, err := strconv.ParseInt(vars["adminPageId"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	adminUserPage := db.AdminUserPage{
		AdminUserId: adminUserId,
		AdminPageId: adminPageId,
	}

	err = db.AdminUserPageDelete(adminUserPage)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func AdminUserPageAdd(w http.ResponseWriter, r *http.Request) {
	var adminUserPage db.AdminUserPage
	err := json.NewDecoder(r.Body).Decode(&adminUserPage)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	isSame, _ := db.IsUserSame(adminUserPage.AdminUserId, helpers.GetLoggedInUser(r))
	if isSame {
		helpers.HttpRespond403(w)
		return
	}

	exists, err := db.DoesAdminUserPageExist(adminUserPage)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if exists {
		w.WriteHeader(http.StatusAlreadyReported)
		return
	}

	adminPage, err := db.GetAdminPage(adminUserPage.AdminPageId)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	if !adminPage.IsActive {
		helpers.HttpWriteError(w, r, fmt.Errorf("cannot add the user page for inactive pages"))
		return
	}

	err = db.AdminUserPageAdd(adminUserPage)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
