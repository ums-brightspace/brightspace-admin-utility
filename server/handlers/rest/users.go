package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"regexp"
	"strings"
)

func UsersGet(w http.ResponseWriter, r *http.Request) {
	userId := strings.TrimSpace(r.URL.Query().Get("userId"))

	err := validateUserId(userId)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	user := db.User{}
	whereClause := "USERNAME = :USERNAME"
	isEmplid := isEmplid(userId)
	if isEmplid {
		whereClause = "EMPLID = :EMPLID"
		user.Emplid = userId
	} else {
		user.Username = userId
	}
	query := "Select EMPLID, USERNAME, EMAIL, LAST_NAME, FIRST_NAME, MIDDLE_NAME From PS_USER Where "

	users, err := getUsers(query+whereClause, user)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if users != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(users)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func getUsers(query string, user db.User) (users []db.User, err error) {
	results, err := helpers.DbConn().NamedQuery(query, &user)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			users = nil
			return
		}
		err = fmt.Errorf("query, <<<%s>>>, failed:%v", query, err)
		return
	}
	defer results.Close()

	var u db.User
	for results.Next() {
		err := results.StructScan(&u)
		if err != nil {
			err = fmt.Errorf("unable to read results with error, %v", err)
			break
		}

		users = append(users, u)
	}

	return
}

func validateUserId(userId string) error {
	if len(userId) == 0 {
		return fmt.Errorf("emplid or username not provided")
	}

	return nil
}

func isEmplid(userId string) bool {
	matched, err := regexp.MatchString("\\d{7}", userId)
	if err != nil {
		return false
	}

	if matched {
		return true
	}

	return false
}
