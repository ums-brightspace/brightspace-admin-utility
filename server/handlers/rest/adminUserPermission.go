package rest

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"strconv"
)

func AdminUserPermissionDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	adminUserId, err := strconv.ParseInt(vars["adminUserId"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	isSame, _ := db.IsUserSame(adminUserId, helpers.GetLoggedInUser(r))
	if isSame {
		helpers.HttpRespond403(w)
		return
	}

	adminPermissionId, err := strconv.ParseInt(vars["adminPermissionId"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	adminUserPermission := db.AdminUserPermission{
		AdminUserId:       adminUserId,
		AdminPermissionId: adminPermissionId,
	}

	err = db.AdminUserPermissionDelete(adminUserPermission)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func AdminUserPermissionAdd(w http.ResponseWriter, r *http.Request) {
	var adminUserPermission db.AdminUserPermission
	err := json.NewDecoder(r.Body).Decode(&adminUserPermission)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	isSame, _ := db.IsUserSame(adminUserPermission.AdminUserId, helpers.GetLoggedInUser(r))
	if isSame {
		helpers.HttpRespond403(w)
		return
	}

	exists, err := db.DoesAdminUserPermissionExist(adminUserPermission)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if exists {
		w.WriteHeader(http.StatusAlreadyReported)
		return
	}

	adminPermission, err := db.GetAdminPermission(adminUserPermission.AdminPermissionId)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
	}
	if !adminPermission.IsActive {
		helpers.HttpWriteError(w, r, fmt.Errorf("cannot add the user permission for inactive permissions"))
		return
	}

	if err = db.AdminUserPermissionAdd(adminUserPermission); err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
