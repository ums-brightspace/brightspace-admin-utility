package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/rest"
	"net/http"
)

func UniversityGetAll(w http.ResponseWriter, r *http.Request) {
	items, err := getAllUniversities()
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}
	if items != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(items)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func UniversityUpdate(w http.ResponseWriter, r *http.Request) {
	var university rest.University
	err := json.NewDecoder(r.Body).Decode(&university)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	vars := mux.Vars(r)
	university.LmsId = vars["id"]

	termTemplate := "TEMPLATE_LMS_ID_" + university.Term

	query := fmt.Sprintf("Update UNIVERSITY_COMPLETED Set %s = :TEMPLATE_LMS_ID Where LMS_ID = :LMS_ID", termTemplate)
	if len(university.TemplateLmsId) == 0 {
		query = fmt.Sprintf("Update UNIVERSITY_COMPLETED Set %s = NULL Where LMS_ID = :LMS_ID", termTemplate)
	}
	sqlResult, err := helpers.DbConn().NamedExec(query, &university)
	if err != nil {
		helpers.HttpWriteError(w, r, fmt.Errorf("error updating university, %s, with error: %+v", university.LmsId, err))
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		helpers.HttpRespond500(w, r, fmt.Errorf("error updating university, %s, with error: %+v", university.LmsId, err))
		return
	}
	if rowsAffected != 1 {
		helpers.HttpRespond500(w, r, fmt.Errorf("error updating university, %s", university.LmsId))
		return
	}

	w.WriteHeader(http.StatusOK)
}

func getAllUniversities() (items []db.University, err error) {
	dbConn := helpers.DbConn()
	query := "Select CODE, NAME, SHORT_NAME, LMS_ID, TEMPLATE_LMS_ID_FALL, TEMPLATE_LMS_ID_SPRING, TEMPLATE_LMS_ID_SUMMER From UNIVERSITY_COMPLETED"
	err = dbConn.Select(&items, query)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			items = nil
			return
		}

		err = fmt.Errorf("query, <<<%s>>>, failed: %+v", query, err)
		return
	}

	return
}
