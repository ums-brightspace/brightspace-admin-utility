package rest

import (
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
)

func AuthedUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Context().Value("userId")
	if userId != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		u := User{Username: fmt.Sprintf("%v", userId)}

		adminUser, err := db.GetAdminUser(u.Username)
		if err != nil {
			helpers.HttpRespond500(w, r, err)
			return
		}
		u.ShowAdminHelpInfo = adminUser.ShowAdminHelpInfo

		perms, err := db.GetAllowedPermissions(adminUser)
		if err != nil {
			helpers.HttpRespond500(w, r, err)
			return
		}

		for _, p := range perms {
			u.Permissions = append(u.Permissions, db.AdminPermission{Name: p.Name})
		}

		pages, err := db.GetAllowedPages(adminUser)
		if err != nil {
			helpers.HttpRespond500(w, r, err)
			return
		}

		for _, p := range pages {
			u.Pages = append(u.Pages, db.AdminPage{RouteName: p.RouteName})
		}

		_ = json.NewEncoder(w).Encode(u)
	} else {
		helpers.HttpLogClientError(r, fmt.Errorf("user not set"))
		w.WriteHeader(http.StatusNotFound)
	}

}

type User struct {
	Username          string               `json:"username"`
	ShowAdminHelpInfo bool                 `json:"showAdminHelpInfo"`
	Permissions       []db.AdminPermission `json:"permissions"`
	Pages             []db.AdminPage       `json:"pages"`
}
