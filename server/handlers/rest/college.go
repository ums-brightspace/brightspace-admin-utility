package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/rest"
	"net/http"
)

func CollegeGetSet(w http.ResponseWriter, r *http.Request) {
	items, err := getColleges(false)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	if items != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(items)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func CollegeGetAll(w http.ResponseWriter, r *http.Request) {
	items, err := getColleges(true)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	if items != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(items)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func getColleges(includeItemsWithNoTemplateCourse bool) (items []db.College, err error) {
	dbConn := helpers.DbConn()
	query := "Select CODE, NAME, PARENT, LMS_ID, nvl(TEMPLATE_LMS_ID_FALL, '<null>') TEMPLATE_LMS_ID_FALL, nvl(TEMPLATE_LMS_ID_SPRING, '<null>') TEMPLATE_LMS_ID_SPRING, nvl(TEMPLATE_LMS_ID_SUMMER, '<null>') TEMPLATE_LMS_ID_SUMMER From COLLEGE_COMPLETED"
	if !includeItemsWithNoTemplateCourse {
		query += " Where TEMPLATE_LMS_ID_FALL Is Not Null Or TEMPLATE_LMS_ID_SPRING Is Not Null Or TEMPLATE_LMS_ID_SUMMER Is Not Null"
	}
	err = dbConn.Select(&items, query)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			items = nil
			return
		}

		err = fmt.Errorf("query, <<<%s>>>, failed with error: %+v", query, err)
	}

	for i := range items {
		if items[i].TemplateLmsIdFall == "<null>" {
			items[i].TemplateLmsIdFall = ""
		}
		if items[i].TemplateLmsIdSpring == "<null>" {
			items[i].TemplateLmsIdSpring = ""
		}
		if items[i].TemplateLmsIdSummer == "<null>" {
			items[i].TemplateLmsIdSummer = ""
		}
	}

	return
}

func CollegeUpdate(w http.ResponseWriter, r *http.Request) {
	var college rest.College
	err := json.NewDecoder(r.Body).Decode(&college)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	vars := mux.Vars(r)
	college.LmsId = vars["id"]

	termTemplate := "TEMPLATE_LMS_ID_" + college.Term

	query := fmt.Sprintf("Update COLLEGE_COMPLETED Set %s = :TEMPLATE_LMS_ID Where LMS_ID = :LMS_ID", termTemplate)
	if len(college.TemplateLmsId) == 0 {
		query = fmt.Sprintf("Update COLLEGE_COMPLETED Set %s = NULL Where LMS_ID = :LMS_ID", termTemplate)
	}
	sqlResult, err := helpers.DbConn().NamedExec(query, &college)
	if err != nil {
		helpers.HttpWriteError(w, r, fmt.Errorf("error updating college, %s, with error: %+v", college.LmsId, err))
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		helpers.HttpRespond500(w, r, fmt.Errorf("error updating college, %s, with error: %+v", college.LmsId, err))
		return
	}
	if rowsAffected != 1 {
		helpers.HttpRespond500(w, r, fmt.Errorf("error updating college, %s", college.LmsId))
		return
	}

	w.WriteHeader(http.StatusOK)
}
