package rest

import (
	"encoding/json"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
)

func GetAllActivePagesWithPermissions(w http.ResponseWriter, r *http.Request) {
	pages, err := db.GetActiveAdminPagesWithPermissions()
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if pages != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(pages)
		return
	}

	w.WriteHeader(http.StatusNotFound)
}
