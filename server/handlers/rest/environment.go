package rest

import (
	"encoding/json"
	"net/http"
	"os"
	"strings"
)

func Environment(w http.ResponseWriter, r *http.Request) {
	env := "Prod"
	if !strings.Contains(os.Getenv("DB_SID"), "prd") {
		env = "Test"
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(map[string]interface{}{"environment": env})
}
