package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"net/http"
	"strconv"
)

type systemEnrollment struct {
	Emplid string `json:"emplid"`
	Role   string `json:"role"`
}

func PutSystemEnrollments(w http.ResponseWriter, r *http.Request) {
	var se systemEnrollment
	err := json.NewDecoder(r.Body).Decode(&se)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = validateEmplid(se.Emplid)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = validateSystemRole(se.Role)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	userId, err := db.GetUserLmsIdByEmplid(se.Emplid)
	if err != nil {
		helpers.HttpRespond403(w)
		return
	}

	userLmsId, err := strconv.Atoi(userId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	umsEnrollments, err := lms.GetOrganizationEnrollments(userLmsId, "ums")
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	} else if len(umsEnrollments) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	universityEnrollments, err := lms.GetOrganizationEnrollments(userLmsId, "university")
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	} else if len(universityEnrollments) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	enrollments := append(universityEnrollments, umsEnrollments[0])
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(enrollments)
}

func validateSystemRole(role string) error {
	switch role {
	case "SA":
	case "CA":
	case "CD":
	case "I":
	case "L":
		return nil
	}

	return fmt.Errorf("unknown role, %s", role)
}

func GetSystemEnrollments(w http.ResponseWriter, r *http.Request) {
	emplid := r.URL.Query().Get("emplid")

	err := validateEmplid(emplid)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	userId, err := db.GetUserLmsIdByEmplid(emplid)
	if err != nil {
		helpers.HttpRespond403(w)
		return
	}

	userLmsId, err := strconv.Atoi(userId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	umsEnrollments, err := lms.GetOrganizationEnrollments(userLmsId, "ums")
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	} else if len(umsEnrollments) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	universityEnrollments, err := lms.GetOrganizationEnrollments(userLmsId, "university")
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	} else if len(universityEnrollments) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	enrollments := append(universityEnrollments, umsEnrollments[0])
	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(enrollments)
}

func GetEnrollmentsByCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	enrollments, err := getEnrollments(code)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if enrollments != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(enrollments)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func getEnrollments(code string) (enrollments []db.Enrollment, err error) {
	dbConn := helpers.DbConn()

	course := db.ParseCourseCode(code)
	query := fmt.Sprintf("Select INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TYPE, EMPLID, FIRST_NAME, LAST_NAME, ENROLL_STATUS, USER_LMS_ID, COURSE_LMS_ID From LMS_ENROLLMENTS_BY_CLASS Where TERM = :TERM And INSTITUTION = :INSTITUTION And CLASS_NUMBER = :CLASS_NUMBER And IS_COMBINED = :IS_COMBINED And SESSION_CODE = :SESSION_CODE")

	results, err := dbConn.NamedQuery(query, &course)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			enrollments = nil
			return
		}
		err = fmt.Errorf("query, <<<%s>>>, failed:%v", query, err)
		return
	}
	defer results.Close()

	var e db.Enrollment
	for results.Next() {
		err := results.StructScan(&e)
		if err != nil {
			err = fmt.Errorf("unable to read results with error, %v", err)
			break
		}

		enrollments = append(enrollments, e)
	}

	return
}
