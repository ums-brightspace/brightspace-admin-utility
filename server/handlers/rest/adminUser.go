package rest

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"strconv"
)

func AdminUserGetById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	adminUser, err := db.GetAdminUserById(id, true, true)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(adminUser)
	return
}

func AdminUserGetAll(w http.ResponseWriter, r *http.Request) {
	adminUsers, err := db.GetAllAdminUsers()
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if adminUsers != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(adminUsers)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func AdminUserDelete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	var adminUser db.AdminUser
	adminUser.Id = id
	dbConn := helpers.DbConn()
	sqlResult, err := dbConn.NamedExec("Delete From ADMIN_USER Where ID=:ID", &adminUser)
	if err != nil {
		err = fmt.Errorf("error deleting admin_user id: %d, and error: %+v", adminUser.Id, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("delete from admin_user error %d rows affected for id: %d, and error: %+v", rowsAffected, adminUser.Id, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	if rowsAffected == 1 {
		w.WriteHeader(http.StatusOK)
		return
	}

	err = fmt.Errorf("delete from admin_user unexpected %d rows affected for id: %d, and error: %+v", rowsAffected, adminUser.Id, err)
	helpers.HttpRespond500(w, r, err)
}

func AdminUserAdd(w http.ResponseWriter, r *http.Request) {
	var adminUser db.AdminUser
	err := json.NewDecoder(r.Body).Decode(&adminUser)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if err = okToAddAdminUser(adminUser); err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	sqlResult, err := helpers.DbConn().NamedExec("Insert Into ADMIN_USER (USERNAME, SHOW_ADMIN_HELP_INFO, IS_ACTIVE) Values (:USERNAME, :SHOW_ADMIN_HELP_INFO, :IS_ACTIVE)", &adminUser)
	if err != nil {
		err = fmt.Errorf("error adding admin user with error: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("add to ADMIN_USER error %d rows affected with error: %+v", rowsAffected, err.Error())

		helpers.HttpWriteError(w, r, err)
		return
	}

	if rowsAffected == 1 {
		w.WriteHeader(http.StatusOK)
		return
	}

	err = fmt.Errorf("add to ADMIN_USER unexpected %d rows affected with error: %+v", rowsAffected, err)
	helpers.HttpRespond500(w, r, err)
}

func AdminUserEdit(w http.ResponseWriter, r *http.Request) {
	var adminUser db.AdminUser
	err := json.NewDecoder(r.Body).Decode(&adminUser)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	vars := mux.Vars(r)
	adminUser.Id, err = strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if err = db.ValidateAdminUser(adminUser, true); err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	sqlResult, err := helpers.DbConn().NamedExec("Update ADMIN_USER Set USERNAME=:USERNAME, SHOW_ADMIN_HELP_INFO=:SHOW_ADMIN_HELP_INFO, IS_ACTIVE=:IS_ACTIVE Where ID=:ID", &adminUser)
	if err != nil {
		err = fmt.Errorf("error updating admin user with error: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("update to ADMIN_USER error %d rows affected with error: %+v", rowsAffected, err.Error())

		helpers.HttpWriteError(w, r, err)
		return
	}

	if rowsAffected == 1 {
		w.WriteHeader(http.StatusOK)
		return
	}

	err = fmt.Errorf("update to ADMIN_USER unexpected %d rows affected with error: %+v", rowsAffected, err)
	helpers.HttpRespond500(w, r, err)
}

func okToAddAdminUser(adminUser db.AdminUser) error {
	if err := db.ValidateAdminUser(adminUser, false); err != nil {
		return err
	}

	dbConn := helpers.DbConn()
	row, err := dbConn.NamedQuery("Select count(*) ROWCOUNT From ADMIN_USER Where USERNAME=:USERNAME", &adminUser)
	if err != nil {
		return fmt.Errorf("error checking if user already exists with error: %+v", err)
	}

	var count int
	if !row.Next() {
		return fmt.Errorf("error getting count of users with error: %+v", err)
	}

	err = row.Scan(&count)
	if err != nil {
		return fmt.Errorf("error counting users adding admin user with error: %+v", err)
	}

	if count > 0 {
		return fmt.Errorf("admin user already exists for: %+v", &adminUser)
	}

	return nil
}
