package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
)

func LinkedCourseDelete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	parentCode := params["parentCode"]
	childCode := params["childCode"]

	err := ValidateCourseCode(parentCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = ValidateCourseCode(childCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	dbConn := helpers.DbConn()
	lcc := db.LinkedCourseChild{
		ParentCode: parentCode,
		Code:       childCode,
	}
	sqlResult, err := dbConn.NamedExec("Delete From LINKED_COURSE_CHILD Where PARENT_CODE = :PARENT_CODE And CODE = :CODE", &lcc)
	if err != nil {
		helpers.HttpWriteError(w, r, fmt.Errorf("error deleting course, %s, from %s, with error: %+v", childCode, parentCode, err))
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		helpers.HttpRespond500(w, r, fmt.Errorf("error deleting course, %s, from %s, with error: %+v", childCode, parentCode, err))
		return
	}

	if rowsAffected != 1 {
		helpers.HttpRespond500(w, r, fmt.Errorf("error deleting course, %s, from %s, with error: unexpected %d rows deleted", childCode, parentCode, rowsAffected))
		return
	}

	deleteParent := "Delete From LINKED_COURSE_PARENT Where CODE = :PARENT_CODE And not exists (Select 1 From LINKED_COURSE_CHILD Where PARENT_CODE = :PARENT_CODE)"
	_, err = dbConn.NamedExec(deleteParent, &lcc)
	if err != nil {
		helpers.HttpLogServerError(r, err)
	}

	w.WriteHeader(http.StatusOK)
	return
}

func LinkedCourseAdd(w http.ResponseWriter, r *http.Request) {
	var lcc db.LinkedCourseChild
	err := json.NewDecoder(r.Body).Decode(&lcc)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = ValidateCourseCode(lcc.ParentCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = ValidateCourseCode(lcc.Code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	parentCourse := db.ParseCourseCode(lcc.ParentCode)
	lcp := db.LinkedCourseParent{
		Institution: parentCourse.Institution,
		Term:        parentCourse.Term,
		ClassNumber: parentCourse.ClassNumber,
		IsCombined:  parentCourse.IsCombined,
		SessionCode: parentCourse.SessionCode,
		Code:        lcc.ParentCode,
	}

	parentInsert := "Insert Into LINKED_COURSE_PARENT (CODE, CLASS_NUMBER, INSTITUTION, TERM, IS_COMBINED, SESSION_CODE) Select :CODE, :CLASS_NUMBER, :INSTITUTION, :TERM, :IS_COMBINED, :SESSION_CODE From dual Where not exists(Select 1 From LINKED_COURSE_PARENT Where CODE = :CODE)"
	dbConn := helpers.DbConn()
	sqlResult, err := dbConn.NamedExec(parentInsert, &lcp)
	if err != nil {
		err = fmt.Errorf("error adding course, %s to %s, with error: %+v", lcc.Code, lcc.ParentCode, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	childCourse := db.ParseCourseCode(lcc.Code)
	lcc.Institution = childCourse.Institution
	lcc.Term = childCourse.Term
	lcc.ClassNumber = childCourse.ClassNumber
	lcc.IsCombined = childCourse.IsCombined
	lcc.SessionCode = childCourse.SessionCode

	sqlResult, err = dbConn.NamedExec("Insert Into LINKED_COURSE_CHILD (PARENT_CODE, CODE, INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE) Values (:PARENT_CODE, :CODE, :INSTITUTION, :TERM, :CLASS_NUMBER, :IS_COMBINED, :SESSION_CODE)", &lcc)
	if err != nil {
		err = fmt.Errorf("error adding course, %s to %s, with error: %+v", lcc.Code, lcc.ParentCode, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("error adding course, %s to %s, with error: %+v", lcc.Code, lcc.ParentCode, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	if rowsAffected == 1 {
		w.WriteHeader(http.StatusOK)
		return
	}

	err = fmt.Errorf("error adding course, %s to %s, with error: unexpected %d rows added", lcc.Code, lcc.ParentCode, rowsAffected)
	helpers.HttpRespond500(w, r, err)
	return
}

func LinkedCourseChildren(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	isChild, err := isCourseAlreadyChild(code)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}
	if isChild {
		helpers.HttpWriteErrorWithCode(w, r, fmt.Errorf("%s is already a child of another course", code), http.StatusConflict)
		return
	}

	courses, err := getChildren(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	_ = json.NewEncoder(w).Encode(courses)
}

func isCourseAlreadyChild(courseCode string) (isChild bool, err error) {
	dbConn := helpers.DbConn()

	var rowCount int
	query := fmt.Sprintf("Select count(*) as row_count From LINKED_COURSE_CHILD Where CODE = '%s'", courseCode)
	row := dbConn.QueryRow(query)
	err = row.Scan(&rowCount)
	if err != nil {
		return
	}

	isChild = false
	if rowCount > 0 {
		isChild = true
	}

	return
}

func getChildren(parentCode string) (courses []db.LinkedCourseChild, err error) {
	dbConn := helpers.DbConn()

	query := fmt.Sprintf("Select PARENT_CODE, INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TITLE, CODE, INSTRUCTOR_NAMES, LMS_ID From ADMIN_LINKED_COURSES_CHILDREN Where PARENT_CODE = '%s'", parentCode)

	results, err := dbConn.Queryx(query)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			courses = nil
			return
		}

		err = fmt.Errorf("query, <<<%s>>>, failed:%v", query, err)
		return
	}
	defer results.Close()

	var c db.LinkedCourseChild
	for results.Next() {
		err := results.StructScan(&c)
		if err != nil {

			err = fmt.Errorf("unable to read results with error, %v", err)
			break
		}

		courses = append(courses, c)
	}

	return
}
