package rest

import (
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"net/http"
	"regexp"
)

type enrollmentActionItem struct {
	CourseCode  string `json:"courseCode"`
	Emplid      string `json:"emplid"`
	CourseLmsId string `json:"courseLmsId"`
	UserLmsId   string `json:"userLmsId"`
}

func LmsEnrollmentPost(w http.ResponseWriter, r *http.Request) {
	lmsEnroll(w, r, "post")
}

func LmsEnrollmentDelete(w http.ResponseWriter, r *http.Request) {
	lmsEnroll(w, r, "delete")
}

func lmsEnroll(w http.ResponseWriter, r *http.Request, action string) {
	var eai enrollmentActionItem
	err := json.NewDecoder(r.Body).Decode(&eai)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = ValidateCourseCode(eai.CourseCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	err = validateEmplid(eai.Emplid)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := db.GetCourseByCode(eai.CourseCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	var lmsEnroll lms.Enrollment
	if action == "post" {
		enroll, err := db.GetUserEnrollmentByClass(eai.Emplid, course)
		if err != nil {
			helpers.HttpWriteError(w, r, err)
			return
		}

		lmsEnroll = lms.Enrollment{
			CourseLmsId: enroll.CourseLmsId,
			UserLmsId:   enroll.UserLmsId,
			Type:        enroll.Type,
		}
		err = lms.PostEnrollment(lmsEnroll)
	} else {
		lmsEnroll = lms.Enrollment{
			CourseLmsId: course.LmsId,
			UserLmsId:   eai.UserLmsId,
		}
		err = lms.DeleteEnrollment(lmsEnroll)
	}
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(lmsEnroll)
}

func validateEmplid(emplid string) error {
	re := regexp.MustCompile(`^\d{7}$`)
	if !re.MatchString(emplid) {
		return fmt.Errorf("invalid emplid, %s", emplid)
	}

	return nil
}
