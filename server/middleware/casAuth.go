package middleware

import (
	"github.com/calbis/cas/v2"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"net/http"
	"net/url"
	"os"
)

type CasAuth struct {
}

var casClient *cas.Client

func (CasAuth *CasAuth) Init(connection string) error {
	authUrl, _ := url.Parse(os.Getenv("AUTH_CONNECTION"))

	var serviceUrl *url.URL = nil
	host := os.Getenv("APP_HOST")
	if len(host) > 0 {
		context := os.Getenv("APP_CONTEXT")
		if len(context) > 0 {
			serviceUrl, _ = url.Parse(host + context)
		} else {
			serviceUrl, _ = url.Parse(host)
		}
		helpers.Logger().Debugw("CAS Init", "serviceUrl", serviceUrl.String())
	}

	httpClient := helpers.HttpClient(false)

	casClient = cas.NewClient(&cas.Options{
		Client:     httpClient,
		URL:        authUrl,
		ServiceURL: serviceUrl,
	})

	return nil
}

func (CasAuth *CasAuth) AuthHandler(next http.Handler) http.Handler {
	return casClient.HandleFunc(func(w http.ResponseWriter, r *http.Request) {
		if !cas.IsAuthenticated(r) {
			cas.RedirectToLogin(w, r)
			return
		}

		next.ServeHTTP(w, r)
		return
	})
}

func (CasAuth *CasAuth) GetUser(r *http.Request) string {
	return cas.Username(r)
}
