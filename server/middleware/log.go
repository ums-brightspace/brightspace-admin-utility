package middleware

import (
	"bytes"
	"encoding/json"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"io"
	"io/ioutil"
	"net/http"
)

func Log(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := r.Context().Value("userId")

		bodyBytes, _ := ioutil.ReadAll(r.Body)
		_ = r.Body.Close()
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		jsonBody := make(map[string]interface{})
		err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&jsonBody)
		if err != nil && err != io.EOF {
			helpers.Logger().Infow("User Route Accessed",
				"user", user,
				"method", r.Method,
				"route", r.RequestURI,
				"body", string(bodyBytes),
				"error", "error reading request body",
				"bodyError", err)
			next.ServeHTTP(w, r)
			return
		}

		helpers.Logger().Infow("User Route Accessed",
			"user", user,
			"method", r.Method,
			"route", r.RequestURI,
			"body", jsonBody)

		next.ServeHTTP(w, r)
	})
}
