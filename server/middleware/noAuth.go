package middleware

import (
	"net/http"
	"os"
)

type NoAuth struct {
}

func (noAuth *NoAuth) Init(connection string) error {
	return nil
}

func (noAuth *NoAuth) AuthHandler(next http.Handler) http.Handler {
	return next
}

func (noAuth *NoAuth) GetUser(r *http.Request) string {
	value, found := os.LookupEnv("SPOOFED_ID")
	if found && len(value) > 0 {
		return value
	}

	return "calvin.bishopii"
}
