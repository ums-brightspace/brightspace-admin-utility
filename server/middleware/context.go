package middleware

import (
	"context"
	"github.com/gorilla/mux"
	"net/http"
)

func Context(r *mux.Router, auth Auth) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			authedUserId := auth.GetUser(r)

			ctx := context.WithValue(r.Context(), "userId", authedUserId)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
