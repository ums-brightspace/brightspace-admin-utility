package server

import (
	"github.com/rs/cors"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/middleware"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func RunServer() {
	auth, err := middleware.CreateAuth(os.Getenv("AUTH_TYPE"))
	if err != nil {
		helpers.Logger().Fatalw("Unable to create authentication", "error", err)
	}

	err = auth.Init(os.Getenv("AUTH_CONNECTION"))
	if err != nil {
		helpers.Logger().Fatalw("Unable to init authentication", "error", err)
	}

	router := NewRouter(os.Getenv("API_URL"), auth)
	router.Use(auth.AuthHandler)
	router.Use(middleware.Context(router, auth))
	router.Use(middleware.Log)
	router.Use(middleware.EnsureAuthorized)

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:3000"},
		AllowedMethods: []string{"GET", "POST", "DELETE", "PUT"},
	})
	handler := c.Handler(router)

	helpers.Logger().Infof("Server startup")

	helpers.Logger().Fatalw("HTTP Server startup failed", "error", http.ListenAndServe(":"+os.Getenv("PORT"), handler))
}

func OnSigKillTerminate() {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-ch
		conn := helpers.DbConn()
		helpers.Logger().Infof("Closing database due to terminate signal")
		err := conn.Close()
		if err != nil {
			helpers.Logger().Infow("Unable to close database", "error", err)
		}
		os.Exit(1)
	}()
}
