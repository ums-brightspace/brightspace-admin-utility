package helpers

import (
	"crypto/tls"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"os"
	"time"
)

func HttpClient(allowCookies bool) *http.Client {
	trans := http.DefaultTransport.(*http.Transport).Clone()
	trans.TLSClientConfig = &tls.Config{InsecureSkipVerify: false}

	timeout, err := time.ParseDuration(os.Getenv("HTTP_CLIENT_TIMEOUT") + "ms")
	if err != nil {
		panic("Bad environment variable, HTTP_CLIENT_TIMEOUT")
	}

	var client http.Client
	if allowCookies {
		jar, err := cookiejar.New(nil)
		if err != nil {
			Logger().Errorf("unable to create a cookie jar")
		}

		client = http.Client{Transport: trans, Timeout: timeout, Jar: jar}
	} else {
		client = http.Client{Transport: trans, Timeout: timeout}
	}
	return &client
}

func HttpRequest(method string, url string, body io.Reader) (data []byte, statusCode int) {
	req, _ := http.NewRequest(method, os.Getenv("LMS_REST_API")+url, body)
	req.Header.Set("Authorization", "Bearer "+os.Getenv("LMS_TOKEN"))
	req.Header.Set("Content-Type", "application/json")

	client := HttpClient(false)
	res, err := client.Do(req)

	statusCode = 600
	if err == nil {
		statusCode = res.StatusCode
	}
	if err != nil || statusCode >= 400 {

		Logger().Infow("HTTP Request Error",
			"method", method,
			"url", url,
			"statusCode", statusCode,
			"error", err)
		data = nil
		return
	}

	defer res.Body.Close()

	data, readErr := ioutil.ReadAll(res.Body)

	if readErr != nil {
		Logger().Infow("HTTP Request Unable to read the body",
			"method", method,
			"url", url,
			"error", readErr)
		data = nil
		return
	}

	return
}

func HttpRespond403(w http.ResponseWriter) {
	http.Error(w, "Forbidden. You are not authorized to use resource", http.StatusForbidden)
}

func HttpRespond500(w http.ResponseWriter, r *http.Request, err interface{}) {
	HttpLogServerError(r, err)
	errString, _ := json.Marshal(err)
	http.Error(w, string(errString), http.StatusInternalServerError)
}

func HttpWriteError(w http.ResponseWriter, r *http.Request, err interface{}) {
	HttpWriteErrorWithCode(w, r, err, http.StatusBadRequest)
}

func HttpWriteErrorWithCode(w http.ResponseWriter, r *http.Request, err interface{}, errorCode int) {
	HttpLogClientError(r, err)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(errorCode)
	_ = json.NewEncoder(w).Encode(err)
}

func HttpLogServerError(r *http.Request, err interface{}) {
	user := r.Context().Value("userId")
	Logger().Infow("Server Error",
		"user", user,
		"route", r.URL.RequestURI(),
		"method", r.Method,
		"error", err)
}

func HttpLogClientError(r *http.Request, err interface{}) {
	user := r.Context().Value("userId")
	Logger().Infow("Client Error",
		"user", user,
		"url", r.URL.RequestURI(),
		"method", r.Method,
		"error", err)
}
