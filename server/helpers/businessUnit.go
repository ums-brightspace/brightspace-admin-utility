package helpers

func GetCampusFromBusinessUnit(businessUnit string) string {
	switch businessUnit {
	case "UMS01":
		return "A"
	case "UMS02":
		return "F"
	case "UMS03":
		return "M"
	case "UMS04":
		return "K"
	case "UMS05":
		return "O"
	case "UMS06":
		return "P"
	case "UMS07":
		return "I"
	default:
		return ""
	}
}
