package helpers

import (
	"path/filepath"
	"runtime"
)

var rootPath = ""

func GetRootPath() string {
	if rootPath == "" {
		_, b, _, _ := runtime.Caller(0)
		rootPath = filepath.Join(filepath.Dir(b), "../..")
	}

	return rootPath
}
