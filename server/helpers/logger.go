package helpers

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"sync"
)

var doOnceLogger sync.Once
var logger *zap.SugaredLogger

func Logger() *zap.SugaredLogger {
	doOnceLogger.Do(func() {
		var config zap.Config
		if os.Getenv("ENVIRONMENT") == "production" {
			config = zap.NewProductionConfig()
		} else {
			config = zap.NewDevelopmentConfig()
		}

		config.Encoding = "json"
		config.OutputPaths = []string{"stdout", "./logs/log.log"}
		config.ErrorOutputPaths = []string{"stderr", "./logs/errorLog.log"}

		config.EncoderConfig.TimeKey = "timestamp"
		config.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
		config.EncoderConfig.MessageKey = "message"
		config.EncoderConfig.LevelKey = "level"

		zap, err := config.Build()
		if err != nil {
			panic(err)
		}

		defer zap.Sync()
		logger = zap.Sugar()
	})

	return logger
}
