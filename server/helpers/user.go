package helpers

import (
	"fmt"
	"net/http"
)

func GetLoggedInUser(r *http.Request) string {
	return fmt.Sprintf("%v", r.Context().Value("userId"))
}
